#ifndef _ALGOTEST_H_
#define _ALGOTEST_H_

#define TEST_LOOPS 100
#define TEST_LENGTH 300
#define TEST_MIN 5
#define TEST_STEP 5
#define TEST_PRINT_SKIP 25

int loopThroughAlgos(char type);

#endif // _ALGOTEST_H_