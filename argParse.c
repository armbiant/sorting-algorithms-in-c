/* ====================================================================================
	    _
	   / \   _ __ __ _ _ __   __ _ _ __ ___  ___   ___
	  / _ \ | '__/ _` | '_ \ / _` | '__/ __|/ _ \ / __|
	 / ___ \| | | (_| | |_) | (_| | |  \__ |  __/| (__
	/_/   \_|_|  \__, | .__/ \__,_|_|  |___/\___(_\___|
	             |___/|_|

	This script is for parsing the arguments that have been input into the program. It
	uses the getopt_long_only() function to do this. This file also defines helpMe(),
	which prints out the help text for the program.

 ==================================================================================== */

#include "algoTable.h"
#include "argparse.h"
#include "common.h"
#include "test.h"

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

// Function to display help when argument --help is used
void helpMe() {
	printf("Usage: sort [options]\n");
	printf("Options:\n");
	printf("\t--help\t\tDisplay this information\n");
	printf("\t--csv <type>\tExports results to a CSV file\n");
	printf("\ttypes:\n");
	printf("\t\t\"b\":\tBasic. Counts the average time to complete each sort over a variety of input sizes\n");
	printf("\t\t\"d\":\tDetailed. Gives you the count of every time a length has been reached\n");
	printf("\t--verbose\tPrints in detail what is going on in each function\n");
	printf("\t--repeats\tUses an array of repeated values instead of all unique integers\n");
	printf("\t--algo <algorithm>\tSorts the list using a specific algorithm. Available algorithms:\n\t\t");
	for (int i=0; i<ALGOTABLE_ROWS; i++) {
		printf("%s\t", listOfAlgos.rows[i].funcName);
		if (!(i%4)) {
			printf("\n\t\t",i);
		}
	}
	printf("\n\t--test <category>\tTests all algorithms in a category for %i runs using different inputs.\n", ALGOTABLE_TYPES-1);
	printf("\tArguments:\n");
	for (int i=0; i<ALGOTABLE_TYPES; i++) {
		printf("\t\t%i: %s\n", i, algoTypes[i]);
	}
}

// Parses input and returns the instructions to follow
uint8_t argParse(int argc, char** argv) {
	int opt, option_index;

	// Struct that contains all the possible args
	// Structure: {*name, has_arg, *flag, flag_val}
	struct option long_options[] = {
		// Flag arguments
		{"verbose",	no_argument,		&v,			1},
		{"v",		no_argument,		&v,			1},
		{"repeats",	no_argument,		&repeats,	1},
		{"r",		no_argument,		&repeats,	1},
		// Non-flag arguments
		{"csv",		required_argument,	0,			'c'},
		{"c",		required_argument,	0,			'c'},
		{"algo",	required_argument,	0,			'a'},
		{"a",		required_argument,	0,			'a'},
		{"help",	no_argument,		0,			'h'},
		{"h",		no_argument,		0,			'h'},
		{"test",	required_argument,	0,			't'},
		{"t",		required_argument,	0,			't'},
		// Error handling
		//{":",		no_argument,		0,			':'},
		// Obligatory entry full of zeros. Don't touch!
		{0,			0,					0,			0}
	};

	// Loop through all arguments
	while(1) {

		// Get the opt
		opt = getopt_long_only(argc, argv, ":vc:h", long_options, &option_index);
		printv("Switching %i(%c)\n", opt, opt);

		// End when we run out of opts
		if (opt == -1) {
			break;
		}
		// Skip flag arguments
		if (opt != ':' && long_options[option_index].flag != 0)
			continue;

		switch(opt) {

			// Set verbosity
			case 'v':
				printf("Verbosity set.\n");
				v = 1;
				break;

			// Export to CSV
			case 'c':
				printv("Found: %s\n", optarg);
				if (optarg[0] == 'b') {
					printf("Doing a basic csv export\n");
					exportType = EXPORT_BASIC;
				} else if (optarg[0] == 'd') {
					printf("Doing a detailed csv export\n");
					exportType = EXPORT_DETAILED;
				} else {
					printf("Please choose only 'b' or 'd'.\n");
					exportType = EXPORT_NONE;
					break;
				}
				break;

			// Get help
			case 'h':
				helpMe();
				exit(EXIT_SUCCESS);

			// Run a specific algorithm once
			case 'a':
				printf("Searching for algorithm \"%s\"...\n", optarg);
				algoFunc = getAlgoIndex(optarg);
				printf("Found %s!\n", listOfAlgos.rows[algoFunc].name);
				break;

			// Run tests
			case 't':
				printf("Starting tests...\n");
				char type = atoi(optarg);
				// Check to see if input is within range
				if (type < ALGOTABLE_TYPES && type >= 0) {
					loopThroughAlgos(type);
				} else {
					printf("Out of range. Please choose a number between 0 and %i\n", ALGOTABLE_TYPES-1);
					exit(EXIT_FAILURE);
				}
				exit(EXIT_SUCCESS);
				break;

			// Error catching
			case ':':
				printf("Argument is missing a value.\n",opt);
				break;
			// Catch invalid option error
			case '?':
				printf("Option not found: %c\n", optopt);
				break;
		}
	}
	return 0;
}