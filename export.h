#ifndef _EXPORT_H_
#define _EXPORT_H_

#define EXPORT_SORT_TESTS 100

#define EXPORT_BASIC_MIN 0
#define EXPORT_BASIC_MAX 1000
#define EXPORT_BASIC_STEP 1
#define EXPORT_BASIC_SIZE 100

#define EXPORT_DETAILED_MIN 10
#define EXPORT_DETAILED_MAX 200
#define EXPORT_DETAILED_STEP 10

#define EXPORT_NONE 0
#define EXPORT_BASIC 1
#define EXPORT_DETAILED 2

// Extern variables

extern char exportType;			// Export type
extern int repeats;				// Whether to randomise inputs or not

// Exporting function
int exportToCsv();

#endif // _EXPORT_H_