/* ====================================================================================
  _____                       _
 | ___/__  ___ __   ___  _ __| |_   ___
 |  _| \ \/ | '_ \ / _ \| '__| __| / __|
 | |___ >  <| |_) | (_) | |  | |_ | (__
 |_____/_/\_| .__/ \___/|_|   \__(_)___|
            |_|

	This file holds the exportToCsv() function, which runs all the algorithms according
	to the specifications defined in algotable.c. It then uses the time taken for each
	algorithm to generate a CSV denoting how long each one took.

 ==================================================================================== */

#include "algoTable.h"
#include "common.h"
#include "data-structures/array.h"
#include "export.h"

#include <stdio.h>
#include <stdlib.h>

// Set some general values


int exportToCsv() {
	int export_max;
	int avg;
	int* plist;
	int psize;
	int listLen;
	int i, j;
	FILE *f;									// Export file
	uint64_t timeTaken;
	uint64_t resultsArray[EXPORT_SORT_TESTS];
	
	f = fopen("out.csv", "w");
	if (f == NULL) {
		printf("Could not open file.\n");
		exit(EXIT_FAILURE);
	}

	// Proceed with export
	// Get max value for export traversals
	printf("Exporting %s traversal\nHow long do you want this export to be?\nRecommended: %i\n",
		(exportType == EXPORT_BASIC ? "basic" : "detailed"),
		(exportType == EXPORT_BASIC ? EXPORT_BASIC_MAX : EXPORT_DETAILED_MAX));
	scanf("%d", &export_max);

	// Print headers
	fprintf(f, ",");
	for (j=0; j<listOfAlgos.len; j++){
		fprintf(f, "%s,", listOfAlgos.rows[j].name);
	}
	fprintf(f, "\n");

	// Loop through various lengths if EXPORT_DETAILED
	// Just loop a bunch of times if EXPORT_BASIC
	printv("Export type: %i\n",exportType);
	for (
		listLen = (exportType == EXPORT_BASIC) ? EXPORT_BASIC_MIN : EXPORT_DETAILED_MIN;
		listLen <= export_max;
		listLen += ((exportType == EXPORT_BASIC) ? EXPORT_BASIC_STEP : EXPORT_DETAILED_STEP)
	) {
		// Print row label
		fprintf(f, "%i,", listLen);

		// Assign list length
		psize = ((exportType == EXPORT_DETAILED) ? listLen : EXPORT_BASIC_SIZE);
		printf("List length: %i%s", listLen, (exportType == EXPORT_BASIC ? "\r" : "\n"));
		plist = malloc(psize * sizeof(int));
		// Populate it
		int t[] = {};
		for (i=0; i<psize; i++) {
			plist[i] = repeats ? randInt (psize-1, i) : i;
		}

		// Loop through algorithms
		for (j=0; j<listOfAlgos.len; j++){

			// Do the shuffling and sorting a bunch of times (EXPORT_DETAILED only)
			for (i=0; i < (exportType == EXPORT_DETAILED ? EXPORT_SORT_TESTS : 1); i++) {
				if (exportType == EXPORT_DETAILED) {
					printf("Loop #%i \r",i);
				}

				// Randomise the list
				shuffle(plist, psize, i);

				// Run the algorithm
				if (psize<=listOfAlgos.rows[j].max) {
					timeTaken = (*listOfAlgos.rows[j].func)(plist, psize);
				} else {
					// The algorithm is too slow to run at this size.
					timeTaken = 0;
				}
				

				// Check for export type
				if (exportType == EXPORT_DETAILED) {
					// Add to array
					resultsArray[i] = timeTaken;
				} else if (exportType == EXPORT_BASIC) {
					// Just print it out
					fprintf(f, "%i,", timeTaken);
				}
			}

			// Write to table
			if (exportType == EXPORT_DETAILED) {
				avg = arrayAverage(resultsArray, EXPORT_SORT_TESTS);
				fprintf(f, "%i,", avg);
				printf("Average time for size %i %s:\t%i\n", psize, listOfAlgos.rows[j].name, avg);
			}

		}
		// Newline
		fprintf(f, "\n");
		free(plist);
	}

	return 0;
}
