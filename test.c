/* ==========================================================================
	 _____         _         
	|_   _|__  ___| |_   ___ 
	  | |/ _ \/ __| __| / __|
	  | |  __/\__ \ |_ | (__ 
	  |_|\___||___/\__(_)___|

	This is a series of functions used to run after the --test argument is
	summoned. They run each algorithm under a set of conditions to make sure
	that it is working correctly. If an error is found at runtime, it returns
	an EXIT_FAILURE response.

 ========================================================================== */

#include "test.h"
#include "algoTable.h"
#include "common.h"
#include "data-structures/array.h"

#include <stdio.h>
#include <stdlib.h>

// Function to test a bunch of times
void testABunchofTimes(uint64_t(*func)(int*, int), int* p, int len, int funcIndex) {
	uint64_t resultsArray[TEST_LOOPS];
	for(int i=1; i<=TEST_LOOPS; i++) {
		printf("Test #%i\r", i);
		shuffle(p, len, i);
		resultsArray[i] = (*func)(p, len);
		 if (!checkList(p, len, 0) || resultsArray[i] <= 0) {
		 	printf("Failed checkList or time taken (%i).\n", resultsArray[i]);
			printList(p, len);
		 	exit(EXIT_FAILURE);
		 }
	}
	printf("Average time for size %i %s: %i", len, listOfAlgos.rows[funcIndex].name, arrayAverage(resultsArray, TEST_LOOPS));
	if (!(len % TEST_PRINT_SKIP) || len == listOfAlgos.rows[funcIndex].max) {
		printf("\n");
	} else {
		printf("\r");
	}
}

// Get the minimum of two values
int algoTestMin(int a, int b) {
	if (a < b) {
		return a;
	} else {
		return b;
	}
}

// Test all algorithms of a cetain type
int loopThroughAlgos(char type) {
	int* p = NULL;
	int len;
	int i, j;
	
	printf("Looping through algos looking for %s...\n", algoTypes[type]);
	// Loop through all algorithms
	for (i=0; i<ALGOTABLE_ROWS; i++) {
		// Check if it is of the correct type
		if (listOfAlgos.rows[i].type == type){
			printf("\nFound %s!\n", listOfAlgos.rows[i].name);

			// Perform basic tests
			printf("-- Performing tests on a list with unique items\n");
			// Loop up to either the max algo rows or TEST_LENGTH
			for(len=TEST_MIN; len<=algoTestMin(listOfAlgos.rows[i].max, TEST_LENGTH); len+=TEST_STEP) {
				p = generateAscendingArray(p, len);
				testABunchofTimes(listOfAlgos.rows[i].func, p, len, i);
			}

			// Perform tests on random integers
			printf("-- Performing tests on a list with random items\n");
			for(len=TEST_MIN; len<=algoTestMin(listOfAlgos.rows[i].max, TEST_LENGTH); len+=TEST_STEP) {
				p = generateRandomArray(p, len);
				testABunchofTimes(listOfAlgos.rows[i].func, p, len, i);
			}

			// Perform tests on a repeating list
			printf("-- Performing tests on a list with repeating items\n");
			for(len=TEST_MIN; len<=algoTestMin(listOfAlgos.rows[i].max, TEST_LENGTH); len+=TEST_STEP) {
				p = generateRepeatingArray(p, len);
				testABunchofTimes(listOfAlgos.rows[i].func, p, len, i);
			}
		}
	}
}