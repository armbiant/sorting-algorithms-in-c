#ifndef _DISTRIBUTIONSORT_H_
#define _DISTRIBUTIONSORT_H_

#include "data-structures/binarr.h"
#include "common.h"
#include "algos/insertionSort.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define RADIX_SORT_BASE 10
#define BEADSORT_FAST 1
#define BEADSORT_SLOW 0
#define BUCKETSORT_BUCKETS 4
#define FLASHSORT_BUCKETS 10
#define PROXMAP_FACTOR 10
#define NUMERIC_TRIE_SIZE 10

// American Flag Sort function
uint64_t americanFlagSort(int *p, int len);

// Bead Sort functions
uint64_t beadSortSlow(int *p, int len);
uint64_t beadSortFast(int *p, int len);

// Burst Sort function
uint64_t burstSort(int *p, int len);

// Burst Sort function
uint64_t bucketSort(int *p, int len);

// Counting Sort function
uint64_t countingSort(int *p, int len);

// Flash Sort function
uint64_t flashSort(int *p, int len);

// Interpolation Sort function
uint64_t interpolationSort(int *p, int len);

// Pigeonhole Sort function
uint64_t pigeonholeSort(int *p, int len);

// Proxmap Sort function
uint64_t proxmapSort(int *p, int len);

// Radix Sort functions
uint64_t radixSortLsd(int *p, int len);
uint64_t radixSortMsd(int *p, int len);

#endif // _DISTRIBUTIONSORT_H_