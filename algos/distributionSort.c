/* ====================================================================================
	 ____  _     _        _ _           _   _             ____             _
	|  _ \(_)___| |_ _ __(_| |__  _   _| |_(_) ___  _ __ / ___|  ___  _ __| |_   ___
	| | | | / __| __| '__| | '_ \| | | | __| |/ _ \| '_ \\___ \ / _ \| '__| __| / __|
	| |_| | \__ | |_| |  | | |_) | |_| | |_| | (_) | | | |___) | (_) | |  | |_ | (__
	|____/|_|___/\__|_|  |_|_.__/ \__,_|\__|_|\___/|_| |_|____/ \___/|_|   \__(_\___|

	These are sorts that are don't use comparisons between items, but instead rely on
	reading the acual values of each item and either performing calculations on it or
	placing it in specificly ordered buckets.

 ==================================================================================== */

#include "distributionSort.h"
#include "data-structures/array.h"
#include "data-structures/piles.h"
#include "data-structures/trie.h"

#include <math.h>

//
// American Flag Sort
//

// This is based on the Dutch Flag Problem but instead of 3 colours, it uses n colours.
// Simply put, it's like MSD Radix Sort, but the sorting is done locally rather than an auxilliary array.
// Time complexity is around O(n+2.35*nlog10(n)),
// a bit slower than radix sort but doesn't use auxiliary memory.

uint64_t americanFlagSortRecursive(int *p, int len, int level) {
	uint64_t c = 0;						// Comparison counter
	int i;								// Loop iterators
	int radixCounter[RADIX_SORT_BASE];	// Array holding the count of each radix value
	int radixOffsets[RADIX_SORT_BASE];	// Array holding the offset of the above array
	int nextOffset = 0;					// Used to check when the next offset is
	static char digits;					// Static var to hold the number of digits in our array
	char b;								// Temp variable to store the target flag colour of the number we're analysing
	int power;							// The power of RADIX_SORT_BASE being used for this depth level
	int nextLen;						// Used to calculate the length of the next recursion

	// Set all values in radixCounter to zero
	memset(&radixCounter, 0, RADIX_SORT_BASE * sizeof(int));
	memset(&radixOffsets, 0, RADIX_SORT_BASE * sizeof(int));

	if (!level) {
		digits = (int)(log(p[arrayMax(p, len)])/log(RADIX_SORT_BASE));
		c += len;
	} else if (level > digits || len <= 0) {
		return c;
	}
	printv("\nDigits: %i, Level: %i, Len: %i\n", digits, level, len);

	// Get the appropriate power of RADIX_SORT_BASE for this level
	power = (int)pow(RADIX_SORT_BASE, digits-level);

	// Count number of objects in each flag colour
	for (i=0; i<len; i++, c++) {
		b = ((p[i] % (power*RADIX_SORT_BASE))/power);
		radixCounter[b]++;
	}

	// Change counts into indices
	for (i=RADIX_SORT_BASE-1; i>=0; i--) {
		radixCounter[i] = i==RADIX_SORT_BASE-1 ? len-radixCounter[i] : radixCounter[i+1]-radixCounter[i];
	}

	// Swap those objects into the correct place
	for (i=0; i<len; i++, c++) {
		if (i == radixCounter[nextOffset]) {
			// We reached the end of this flag colour
			i += radixOffsets[nextOffset]-(i>0?1:0);
			printv("Next offset: %i,%i\n", radixCounter[nextOffset], len);
			nextOffset++;
		}
		b = ((p[i] % (power*RADIX_SORT_BASE))/power);
		printv("I: %i(%i), Power: %i, B: %i+%i\n", i, p[i], power, radixCounter[b], radixOffsets[b]);
		
		printv("I: %i, Sum: %i, Len: %i, counter: %i\n", i, radixCounter[b]+radixOffsets[b], len,radixCounter[b+1]);

		// Perform some checks to make sure that a swap is necessary
		if (radixCounter[b]+radixOffsets[b] > i && // Check if it's already in place
			radixCounter[b]+radixOffsets[b] < len && // Check if it's reached the end of the list
			radixCounter[b]+radixOffsets[b] < (b+1 == RADIX_SORT_BASE ? len : radixCounter[b+1])) { // Check if it's reached the next offset

			p[i] ^= p[radixCounter[b]+radixOffsets[b]] ^= p[i] ^= p[radixCounter[b]+radixOffsets[b]];
			printv("Swapping %i(%i) to index %i(%i) in group #%i\n",i,p[i],radixCounter[b]+radixOffsets[b],p[radixCounter[b]+radixOffsets[b]],b);
			i--;
		}

		// Make sure it doesn't overflow into the next offset
		if(radixCounter[b]+radixOffsets[b] >= len ||
			radixCounter[b]+radixOffsets[b] >= (b+1 == RADIX_SORT_BASE ? len : radixCounter[b+1])) {
			continue;
		}
		radixOffsets[b]++;
	}

	// List is sorted by radix. Now do it again recursively
	for (i=0; i<RADIX_SORT_BASE; i++, c++) {
		nextLen = radixCounter[b]+radixOffsets[b];
		printv("B: %i(%i+%i)\n", b, radixCounter[b], radixOffsets[b]);
		printv("L: %i, I: %i, Radix counter: %i, Radix offset: %i, nextLen: %i\n", level, i, radixCounter[i], radixOffsets[i], nextLen);
		c += americanFlagSortRecursive(&p[radixCounter[i]], radixOffsets[i], level+1);
	}

	return c;
}

uint64_t americanFlagSort(int *p, int len) {
	uint64_t c = 0;		// Comparison counter
	c += americanFlagSortRecursive(p, len, 0);
	printv("American Flag sort checks: %i\n", c);
	return c;
}

//
// Bead Sort
//

// A natural sorting algorithm that uses gravity to sort a 2D binary array
// Each bit in the binary array is done one at a time, cascading bits towards the bottom
// Fast bead sort time complexity: O(2.5n^2+n)
// Slow bead sort time complexity: O(0.5(n^3+n^2)+n)
// Fast method drops bits to the bottom in one step, slow method drops them one row at a time

uint64_t beadSortHelper(int *p, int len, char type) {
	uint64_t c = 0;		// Comparison counter
	int max_value;		// Highest value in the array
	binarr* abacus;		// Array holding all the bianry arrays
	int i, j, k;		// Loop iterators
	int* lowest;		// Lowest value in each column;

	// Get max value
	max_value = p[arrayMax(p, len)];
	c += len;
	printv("Max value: %i\n", max_value);

	// Allocate memory for abacus
	lowest = calloc(max_value, sizeof(int));
	abacus = malloc(len * sizeof(binarr));
	for (i=0; i<len; i++) {
		c++;
		binarrInit(&abacus[i], len);
		// Set abacus digits
		for (j=0; j<p[i]; j++) {
			c++;
			binarrSet(&abacus[i], j, 1);
		}
	}

	// Print initial status of abacus
	// if (v) { binarr2dPrint(abacus, len); }
	
	if (type) {
		// Fast version
		// Drop the beads down
		// We're nesting the loops in reverse order to 'rotate' the abacus
		for (i=max_value-1; i>=0; i--) {
			// Loop though digits
			printv("I: %i\n", i);
			for (j=len-1; j>=0; j--) {
				c++;
				printv("J: %i:%i\n", len-1-j, lowest[i]);
				// Check if there is an empty lower value
				if (binarrGet(&abacus[j], i)) {
					// Are all up until now filled?
					if (len-1-j != lowest[i]) {
						// No? Then swap them
						printv	("Swap! %i:%i\n", j-lowest[i], i);
						binarrSet(&abacus[len-1-lowest[i]], i, 1);
						binarrSet(&abacus[j], i, 0);

					}
					lowest[i]++;

				}
			}

			// Optionally print each step
			// if (v) { printv("\n"); binarr2dPrint(abacus, max_value); }
		}
	} else {
		// Slow version
		// Loop through list items
		for (i=0; i<len-1; i++) {
			// Loop through items again, cascade-style
			for (j=0; j<len-1-i; j++) {
				// Loop though digits
				for (k=0; k<max_value; k++) {
					c++;
					// Check if digit below is empty
					if (binarrGet(&abacus[j], k) && !binarrGet(&abacus[j+1], k)) {
						binarrSet(&abacus[j], k, 0);
						binarrSet(&abacus[j+1], k, 1);
					}
				}
			}

			// Optionally print each step
			// if (v) { binarr2dPrint(abacus, max_value); }
		}
	}

	// Print final status of abacus
	// if (v) { binarr2dPrint(abacus, max_value); }

	// Put back into array
	for (i=0; i<len; i++) {
		// Reset it
		p[i] = 0;
		for (j=0; j<max_value; j++) {
			c++;
			p[i] += binarrGet(&abacus[i], j);
		}
		
		// Free each binary array
		binarrFree(&abacus[i]);
	}

	return c;
}

uint64_t beadSortFast (int *p, int len) {
	uint64_t c;
	c = beadSortHelper(p, len, BEADSORT_FAST);
	printv("Fast bead sort checks: %i\n", c);
	return c;
}

uint64_t beadSortSlow (int *p, int len) {
	uint64_t c;
	c = beadSortHelper(p, len, BEADSORT_SLOW);
	printv("Slow bead sort checks: %i\n", c);
	return c;
}

//
// Bucket Sort
//

// Splits the array into equally sized buckets
// Then sorts those buckets using Insertion Sort
// Time complexity O(5.2n)

uint64_t bucketSort(int *p, int len) {
	uint64_t c = 0;		// Comparison counter
	int i, j, k;		// Loop iterator
	int pos;			// Placeholder for the bucket number in which it needs to be placed
	int min, max;		// Minimum/maximum value
	piles stacks;		// Piles object to hold the buckets

	// End recursion for small lists
	if (len <= 1) {
		return c;
	} else if (len <= BUCKETSORT_BUCKETS) {
		return insertionSort(p, len);
	}

	// Allocate memory for the buckets
	min = p[arrayMin(p, len)];
	max = p[arrayMax(p, len)];
	pilesInit(&stacks);
	manyNewPiles(&stacks, (max-min)/BUCKETSORT_BUCKETS+1);
	c += len*2;

	
	// Put them in the buckets
	for (i=0; i<len; i++) {
		pos = (p[i]-min) / BUCKETSORT_BUCKETS;
		addToPile(&stacks, pos, p[i]);
		c++;
	}

	k = 0;
	for (i=0; i<(max-min)/BUCKETSORT_BUCKETS+1; i++) {
		// Perform Insertion Sort on each bucket
		c += insertionSort(stacks.piles[i], stacks.pilesLen[i]);

		// Place the buckets back into the array
		for (j=0; j<stacks.pilesLen[i]; j++) {
			p[k++] = stacks.piles[i][j];
			c++;
		}
	}

	// Free all that data
	clearPile(&stacks);
	printv("Bucket sort checks: %i\n", c);
	return c;
}

//
// Burst Sort
//

// This is a sorting algorithm that is used to sort strings using trie structures.
// Since this program only sorts integers, we're just splitting ints like MSD Radix Sort
// Time complexity O(n*(ceil(log(n))*2-1)), but that's just using estimations.

uint64_t burstSort(int *p, int len) {
	uint64_t c = 0;		// Comparison counter
	trie t;
	int i, j;
	int digits;

	// Get max
	digits = (int)(log(p[arrayMax(p, len)])/log(NUMERIC_TRIE_SIZE));
	c += len;

	// Initialise the trie
	trieInit(&t, NUMERIC_TRIE_SIZE, 0);

	// Insert into trie
	c += digits * len;
	for (i=0; i<len; i++) {
		trieInsert(&t, p[i], digits);
	}

	// Pull back from trie
	c += digits * len;
	trieToArray(&t, p, digits);

	// Clean things up
	trieClear(&t);
	printv("Burst sort checks: %i\n", c);
	return c;
}

//
// Counting Sort
//

// This algorithm first finds the largest element in the array,
// and creates a 2nd array with all values from zero to the max value.
// It then traverses the array and counts a tally of each number it comes across.
// Time complexity O(2n+k), where k is the max value.

uint64_t countingSort(int *p, int len) {
	uint64_t c = 0;		// Comparison counter
	int i, j;			// Loop iterators
	int max;			// Max value
	int* counter;		// Counting array

	// Find largest value in the array
	max = p[arrayMax(p, len)];
	c += len;
	printv("Largest element in array: %i\n", max);

	// Create an array with everything set to zero
	counter = calloc(max+1, sizeof(int));

	// Loop through each item
	for (i=0; i<len; i++, c++) {
		// Increase the count for that item
		counter[p[i]]++;
	}

	// Recreate the array
	j = 0;	// This is to keep track of our position in the original array
	for (i=0; i<=max; i++, c++) {
		// Reduce the counter
		while (counter[i]--) {
			p[j++] = i;
		}
	}

	free(counter);
	printv("Counting sort checks: %i\n", c);
	return c;
}

//
// Flash Sort
//

// Kind of like a mixture between American Flag sort, Pigeonhole Sort and cycle sort
// Counts the number of elements in each bucket and then cycle swaps them back in
// Time complexity O(0.03n^2+5.1n+35) for 10 buckets, with a decreasing n^2 coefficient as buckets increase

int getFlashBucket(int val, int min, int max) {
	int bucket = (int)((FLASHSORT_BUCKETS-1) * (float)(val-min) / (float)(max-min));
	//printf("Bucket for %i: %i/%i=#%i\n", val,(val-min), (max-min), bucket);
	return bucket;
}


uint64_t flashSort(int *p, int len) {
	uint64_t c = 0;		// Comparison counter
	int min, max;		// Min/Max values of array
	int range;			// Range between min and max
	int* bucketCount;	// Array holding the count of elements in each bucket
	int* bucketAbsolute;// Array holding the correct bucket for each element
	int bucketTemp;		// Temporary variable to keep the code cleaner
	int i, j, k;		// Loop iterators
	int temp, tempCount;// For the repositioning of items

	// Get min/max indices in the array
	min = p[arrayMin(p, len)];
	max = p[arrayMax(p, len)];
	range = max - min + 1;
	printv("Min: %i, Max: %i, Range: %i\n", min, max, range);
	c += len * 2;

	// Count the number of elements in each bucket
	bucketCount = calloc(FLASHSORT_BUCKETS, sizeof(int));
	for (i=0; i<len; i++) {
		c++;
		bucketCount[getFlashBucket(p[i], min, max)]++;
	}

	// Change to a prefix sum
	for (i=1; i<FLASHSORT_BUCKETS; i++) {
		c++;
		bucketCount[i] += bucketCount[i-1];
	}
	for (i=0; i<FLASHSORT_BUCKETS; i++) {
		c++;
		bucketCount[i]--;
	}

	// Create a copy of bucketCount
	bucketAbsolute = malloc((FLASHSORT_BUCKETS+1) * sizeof(int));
	bucketAbsolute[0] = -1;
	memcpy(bucketAbsolute+1, bucketCount, FLASHSORT_BUCKETS * sizeof(int));

	// Place elements back into their bucket
	temp = p[0];
	i = tempCount = 0;
	while (tempCount < len) {
		c++;
		printv("\nIndex: %i, value: %i\n", i, p[i]);
		bucketTemp = getFlashBucket(p[i], min, max);
		printv("%i belongs in %i (group %i)\n", p[i], bucketCount[bucketTemp], bucketTemp);
		// Check if it is in the right position
		if (i <= bucketAbsolute[bucketTemp] || i > bucketAbsolute[bucketTemp+1]) {
			// It isn't, swap it
			printv("Swapping %i with item in pos #%i(%i)\n", p[i], bucketCount[bucketTemp], p[bucketCount[bucketTemp]]);
			p[i] ^= p[bucketCount[bucketTemp]] ^= p[i] ^= p[bucketCount[bucketTemp]];
			printv("Temp: %i\n", p[i]);
			bucketCount[bucketTemp]--;
		} else {
			// End of cycle
			printv("\nEnd of cycle. Searching for new item...\n");
			for (tempCount; tempCount<len; tempCount++) {
				c++;
				bucketTemp = getFlashBucket(p[tempCount], min, max);
				if (i <= bucketAbsolute[bucketTemp] || i > bucketAbsolute[bucketTemp+1]) {
					printv("Found an empty one! %i->%i\n", p[tempCount], bucketTemp);
					i = tempCount;
					break;
				} else {
					printv("%i(%i) is in its right place.\n", tempCount, p[tempCount]);
				}
			}
		}
	}

	c += insertionSort(p, len);

	// Clean things up
	free(bucketCount);
	free(bucketAbsolute);
	printv("Flash sort checks: %i\n", c);
	return c;
}

//
// Interpolation Sort
//

// Like counting sort, but uses the interpolation formula to designate array position
// Best for arrays with large gaps in input size
// Time complexity O(5n)

uint64_t interpolationSort(int *p, int len) {
	uint64_t c = 0;				// Comparison counter
	int* unSortedLengths;		// Length of the unsorted portion of the array
	int unSortedLengthsLength;	// Length of the above array
	int iteration=0;			// Counter for the interations of the main loop
	int end = len;				// Used in sub-function
	int size;					// Current size we're working with in this loop
	int start;					// Start of iterator
	int min, max;				// Index for the current min/max values
	int i, j;					// Loop iterator
	int b;						// Temp value for bucket storage
	piles buckets;				// Buckets object

	// Initialise the unSortedLengths array to be the length of the input array
	unSortedLengthsLength = 1;
	unSortedLengths = malloc(unSortedLengthsLength * sizeof(int));
	unSortedLengths[0] = end;

	// Check to see if the length of unsorted items is cleared
	while (unSortedLengthsLength) {
		printv("Iteration #%i\n", iteration++);
		// Pop size from array
		size = unSortedLengths[unSortedLengthsLength-1];
		unSortedLengthsLength--;
		printv("Len %i\n", unSortedLengthsLength);
		if (unSortedLengthsLength) {
			printv("Resizing...\n");
			unSortedLengths = realloc(unSortedLengths, unSortedLengthsLength * sizeof(int));
		}
		printv("Size: %i\n", size);

		// Get start, min and max
		start = end - size;
		printv("Start: %i\n", start);
		min = max = start;

		// Find the min and max value for this loop
		for (i=start; i<end; i++) {
			c++;
			min = p[i] < p[min] ? i : min;
			max = p[i] > p[max] ? i : max;
		}
		printv("Min: %i(%i), max: %i(%i)\n", min, p[min], max, p[max]);

		// Check if we have nothing left to sort
		if (min == max) {
			// End things
			end -= size;
		} else {
			// Yes, let's sort some more...
			// Generate a 2D array
			pilesInit(&buckets);
			for (i=0; i<size; i++) {
				c++;
				newPile(&buckets);
			}

			// Place items into buckets
			for (i=start; i<end; i++) {
				c++;
				// Calculate bucket index by using interpolation formula
				b = (int)((float)(p[i]-p[min] ) / (p[max]-p[min]) * (size - 1));
				printv("B: %i\n", b);

				// Push it into the bucket
				addToPile(&buckets, b, p[i]);
			}

			// Put them back into the array
			for (i=0; i<size; i++) {
				c++;
				printv("I: %i\n",i);
				if (buckets.pilesLen[i]) {
					// Insert back into array
					for (j=0; j<buckets.pilesLen[i]; j++) {
						p[start++] = buckets.piles[i][j];
					}
					// Push current bucketlen into unSortedLengths
					unSortedLengthsLength++;
					unSortedLengths = realloc(unSortedLengths, unSortedLengthsLength * sizeof(int));
					unSortedLengths[(unSortedLengthsLength-1)] = buckets.pilesLen[i];
				}
			}

			// Free the piles struct
			clearPile(&buckets);
		}
	}

	// Finish things
	free(unSortedLengths);
	printv("Interpolation sort checks: %i\n", c);
	return c;
}

//
// Pigeonhole Sort
//

// Very similar to Counting Sort, but creates an array of length 'range'
// It stores items themselves in the auxiliary array rather than just their count
// Time complexity O(r-4n) where `r` is range

uint64_t pigeonholeSort(int *p, int len) {
	uint64_t c = 0;		// Comparison counter
	int max, min;		// Max/min values in the array
	int range;			// Range between the largest and smallest values
	int i, j, k;		// Loop iterators
	piles stacks;		// Piles object to hold the buckets

	// Get min, max and range
	min = arrayMin(p, len);
	max = arrayMax(p, len);
	range = p[max] - p[min] + 1;
	printv("Min: %i, Max: %i, Range: %i\n", p[min], p[max], range);
	c += len * 2;

	// Initialise the pigeonholes
	pilesInit(&stacks);
	for (i=0; i<range; i++) {
		newPile(&stacks);
	}

	// Loop through elements and place them in their pigeonholes
	for (i=0; i<len; i++) {
		addToPile(&stacks, p[i]-p[min], p[i]);
		c++;
	}

	// Put objects back into the array
	k = 0; // Index of original array
	// Loop through pigeonholes
	for (i=0; i<range; i++) {
		// Loop through each item in the pigeonhole
		for (j=0; j<stacks.pilesLen[i]; j++) {
			// Place it in the correct position and increase index by one
			p[k++] = stacks.piles[i][j];
			c++;
		}
		// Account for range
		c++;
	}

	// Free the pigeonholes
	clearPile(&stacks);

	printv("Pigeonhole sort checks: %i\n", c);
	return c;
}

//
// Proxmap Sort
//

// This version has been adapted for use with integers, while it is best suited for floats/decimals
// Basically like Flash Sort but uses more memory in exchange for a faster speed.
// Time complexity O(7.25n)

uint64_t proxmapSort(int *p, int len) {
	uint64_t c = 0;		// Comparison counter
	int* hitCount;		// Array holding the count of ocurrences in each bucket
	int* proxMap;		// Array holding the starting index of each bucket
	int* location;		// Holds the target index where the item at that index should go
	int* p2;			// Auxiliary copy of the initial array on which to place items before fianl insertion
	int i, j;			// Loop iterators
	int subLen;			// Length of the bucket array
	int max;			// Max value in the array
	int temp;			// Temp value holder

	// Get max
	max = p[arrayMax(p, len)];
	c += len;

	// Initialise the arrays
	subLen = max / PROXMAP_FACTOR + 1;
	hitCount = calloc(subLen, sizeof(int));
	proxMap = malloc(subLen * sizeof(int));
	location = malloc(len * sizeof(int));
	p2 = malloc(len * sizeof(int));
	for (i=0; i<subLen; i++) {
		proxMap[i] = -1;
		location[i] = -1;
		c++;
	}

	// Get count of each bucket and put it into hitCount
	for (i=0; i<len; i++) {
		hitCount[p[i]/PROXMAP_FACTOR]++;
		c++;
	}

	// Calculate proxmap
	temp = 0;
	for (i=0; i<subLen; i++) {
		if (hitCount[i]) {
			temp += hitCount[i];
			proxMap[i] = temp;
		}
		c++;
	}

	// Calculate target locations for each item
	for (i=0; i<len; i++) {
		// Calculate the location by substracting the offset from the proxmap
		location[i] = proxMap[p[i]/PROXMAP_FACTOR] - hitCount[p[i]/PROXMAP_FACTOR];
		// Decrease offset by one
		hitCount[p[i]/PROXMAP_FACTOR]--;
		c++;
	}

	// Place them in the auxiliary array
	for (i=0; i<len; i++) {
		p2[location[i]] = p[i];
		c++;
	}

	// Copy the auxiliary array back to the main array
	for (i=0; i<len; i++) {
		p[i] = p2[i];
		c++;
	}

	c += insertionSort(p, len);

	// Clean things up
	free(hitCount);
	free(proxMap);
	free(location);
	free(p2);
	printv("Proxmap sort checks: %i\n", c);
	return c;
}

//
// Radix Sort
//

// The LSD algorithm works like Counting Sort, but splits the code into buckets
// These buckets correspond to each possible digit in each decimal place the numbers have
// Time complexity O(n+2*nlog10(n)), these are not comparisons, only reads and insertions

uint64_t radixSortLsd(int *p, int len) {
	uint64_t c = 0;		// Comparison counter
	char digits, d;		// Number of digits + iterator
	int i, j;			// Loop iterator
	int max;			// Highest value in array
	int power;			// The current power of RADIX_SORT_BASE we're using
	int b;				// Index for reinsertion
	piles buckets;		// Piles data structure

	// Get digits
	max = arrayMax(p, len);
	c += len;
	digits = (int)(log(p[max])/log(RADIX_SORT_BASE));
	printv("Max: %i, digits: %i\n", max, digits);

	// Loop though each digit
	for (d=0; d<=digits; d++) {

		// Prepare buckets
		pilesInit(&buckets);
		manyNewPiles(&buckets, RADIX_SORT_BASE);

		// Calculate the power for this iteration
		power = (int)pow(RADIX_SORT_BASE, d+1);

		// Insert into buckets
		for (i=0; i<len; i++, c++) {
			b = (p[i] % power) / (power/RADIX_SORT_BASE);
			addToPile(&buckets, b, p[i]);
		}

		// Place back in the list
		c += pilesToArray(&buckets, p);

		// Free the memory
		clearPile(&buckets);
	}

	printv("LSD Radix sort checks: %i\n", c);
	return c;
}

// Instead of working from right to left, MSD works from left to right
// This recursive function is useful for strings and other large elements
// Time complexity is the same as LSD Radix sort, O(n+2*nlog10(n))

uint64_t radixSortMsdRecursive(int *p, int len, char level) {
	uint64_t c = 0;					// Comparison counter
	int i, j;						// Loop iterator
	piles buckets;					// Bucket in which sorted items are stored
	static char digits;				// Static var to hold the number of digits in our array
	int power, b;					// The current power of RADIX_SORT_BASE we're using

	// Return if there's nothing to sort
	if (len <= 1) {
		return(c);
	}

	// Get digits only on the first run
	if (!level) {
		c += len;
		digits = (int)(log(p[arrayMax(p, len)])/log(RADIX_SORT_BASE));
	}

	// Prepare buckets
	pilesInit(&buckets);
	manyNewPiles(&buckets, RADIX_SORT_BASE);

	// Get the highest power of our base
	power = (int)pow(RADIX_SORT_BASE, digits-level);

	// Add items to buckets
	for (i=0; i<len; i++, c++) {
		b = ((p[i] % (power*RADIX_SORT_BASE))/power);
		printv("B: %i -> %i\n", p[i], b);
		addToPile(&buckets, b, p[i]);
	}

	// Perform recursively for the next power
	for (i=0, b=0; i<RADIX_SORT_BASE; i++) {
		// Check if bucket is empty or if bucket is already on the lowest power
		if (buckets.pilesLen[i] && power > 1) {
			c += radixSortMsdRecursive(buckets.piles[i], buckets.pilesLen[i], level+1);
		}
		
	}

	// Place the elements back into the array
	c += pilesToArray(&buckets, p);

	// Free allocated memory
	clearPile(&buckets);
	return c;
}

uint64_t radixSortMsd(int *p, int len) {
	uint64_t c = 0;					// Comparison counter
	c += radixSortMsdRecursive(p, len, 0);
	return c;
}