/* ====================================================================================
	 ___                     _   _             ____             _
	|_ _|_ __  ___  ___ _ __| |_(_) ___  _ __ / ___|  ___  _ __| |_   ___
	 | || '_ \/ __|/ _ | '__| __| |/ _ \| '_ \\___ \ / _ \| '__| __| / __|
	 | || | | \__ |  __| |  | |_| | (_) | | | |___) | (_) | |  | |_ | (__
	|___|_| |_|___/\___|_|   \__|_|\___/|_| |_|____/ \___/|_|   \__(_\___|

	These sorts essentially take one item and insert it into its correct position in an
	already-sorted sublist.It can also be that the algorithm sorts the array into a
	different structure and then inserts it back into the array by traversing the 
	structure.

 ==================================================================================== */

#include "insertionSort.h"
#include "common.h"
#include "data-structures/tree.h"
#include "data-structures/piles.h"

#include <math.h>
#include <stdint.h>
#include <stdlib.h>

//
// Gnome  Sort
//

// Moves towards the right and swaps smaller pots to the left.
// Has the efficiency of an insertion sort, but has to traverse rightward, making twice as ineffective.
// Time complexity becomes that of bubblesort: O((n^2 -x)/2 - <perm>)

uint64_t gnomeSort(int *p, int len) {
	int i = 1;
	uint64_t c = 0;
	while (1) {
		if (i == 0) {
			// Reached left end, move right and skip
			i++;
			continue;
		} else if (p[i-1] > p[i] && i != len) {
			// Swap pots and move leftwards
			p[i] ^= p[i-1] ^= p[i] ^= p[i-1];
			i--;
		} else if (i < len) {
			// Move rightwards
			i++;
		} else {
			// Gnome has move too far right. List is sorted!
			break;
		}
		c++;
	}
	printv("Gnome sort checks: %i\n", c);
	return c;
}

//
// Insertion Sort
//

// Moves from right to left 
// Moves leftward and inserts it in the correct place in the sorted list
// Completes in O((n^2-n)/4) time

uint64_t insertionSort(int *p, int len) {
	int i, j;
	uint64_t c = 0;
	for (i=1; i<len; i++){
		for (j=i; j>0; j--) {
			c++;
			// Is this one in the wrong position compared to the next one?
			if (p[j] < p[j-1]){
				// Swap them!
				p[j] ^= p[j-1] ^= p[j] ^= p[j-1];
			} else {
				break;
			}
		}
	}
	printv("Insertion sort checks: %i\n", c);
	return c;
}

//
// Library Sort (sort of)
//

// Takes a small sorted list and adds small gaps (represented by '-1') between each item
// Then uses a binary search to place each new item in the correct gap
// When a gap is occupied, it reshuffles by compressing the remaining gaps and then re-gapping
// Time complexity O((n^2+n)/2, a bit worse than bubblesort
// True librarySort doesn't reshuffle every time a conflict is found, it just uses insertionsort to find a nearby gap


uint64_t librarySort(int *p, int len) {
	uint64_t c = 0;
	int gap = 1;	// Gap size
	int* s;			// Array where the work is going to be done
	int i, j;		// Loop iterators
	gap++;			// Add one to gap to account for the actual list
	int chunk = 2;	// Chunk size to pull from the p array at a time
	int prev;		// Previous reference in the library for the binary search
	int bingap = chunk/2;	// Used for reducing the size of binary search gaps
	char fullLib = 1;	// Used to check if there are no more gaps in the library
	int slen;		// Length of the current S array's useful area

	// Make a sorting array big enough to hold all gaps
	s = malloc(gap * len * sizeof(int));
	// Fill it with -1's
	for (i=0; i<gap*len; i++) {
		s[i] = -1;
	}

	// Sort 2 items to start using a simple insertion sort
	c += insertionSort(p+len-chunk, chunk);

	// Copy those items to the end of the S array
	for (i=len-1; i>=len-chunk; i--) {
		s[len+i] = p[i]; 
	}

	// Close the gaps

	// Gap it up
	for (i=chunk; i>0; i--) {
		s[len*gap-i] ^= s[len*gap-i*gap] ^= s[len*gap-i] ^= s[len*gap-i*gap];
	}

	while (chunk < len) {
		// Start inserting new items
		slen = (chunk) * gap;
		while (fullLib && chunk < len) {
			// Next item
			chunk++;
			bingap = slen/4;
			prev = len*gap-slen+bingap*gap;
	
			// Find location
			for (bingap=slen/4; bingap>=0; bingap) {
				c++;
				if (p[len-chunk] > s[prev]) {
					// New item is larger, check next item to the right
					if (bingap > 0 && prev + bingap*gap < len*gap) {
						// Increase accuracy by removing bingap and moving prev to the new position
						prev += bingap*gap;
						bingap--;
					} else if (prev + bingap*gap >= len*gap) {
							// It's out of bounds, only cut bingap down
							bingap--;
					} else {
						// Last iteration of binary search, insert into right gap
						if (s[prev+1] == -1 && prev+1 < len*gap) {
							s[prev+1] = p[len-chunk];
							break;
						} else {
							// List is full. Reshuffle.
							fullLib = 0;
							break;
						}
					}
				} else {
					// New item is smaller, check next item to the left
					if (bingap > 0) {
						// Increase accuracy by removing bingap and moving prev to the new position
						if(prev-bingap*gap >= 0) {
							// It's in bounds, also move prev value along
							prev -= bingap*gap;
						}
						bingap--;
					} else {
						// Last iteration of binary search, insert into left gap
						if (s[prev-1] == -1) {
							s[prev-1] = p[len-chunk];
							break;
						} else {
							// List is full. Reshuffle.
							fullLib = 0;
							break;
						}
					}
				}
			}
		}

		// Compress the list, moving all '-1' to the left in O(n) time
		j = len*gap-1; // J to keep track of where the last empty cell was
		for (i=len*gap-1; i>=len*gap-slen-2; i--, c++) {
			if (s[i] >= 0) {
				// This is a real number
				if (s[j] == -1) {
					// Swap  with the last J if it is -1
					s[i] ^= s[j] ^= s[i] ^= s[j];
					j--;
				}
			} else {
				// This is a gap
				if (s[j] == -1) {
					// Already have a J
				} else {
					// No J set yet. Set it
					j = i;
				}
			}
		}
		
		// Check for completeness
		if (chunk >= len && fullLib) {
			// End the sort
			// Put it back into the p array
			for (i=len-1; i>=0; i--) {
				p[i] = s[len+i];
			}
		} else {
			// Expand by adding gaps in between each item
			for (i=len*gap-chunk; i<len*gap; i++) {
				s[i] ^= s[len*gap-(len*gap-i)*gap] ^= s[i] ^= s[len*gap-(len*gap-i)*gap];
			}
			chunk--;
		}
		// Reset fullLib to true so that we can continue
		fullLib = 1;
	}

	printv("Library sort checks: %i\n", c);
	free(s);
	return c;
}

//
// Patience Sort
//

// Works like a game pf Patience/Solitaire. It puts ascending elements in a pile,
// a new pile is added if the next is lower than the max of all other piles.
// The ordered piles are then put together with a k-way merge to get the final array.
// Time complexity: O(nlog(n)*1.5). 1 for the piling, 0.5 for the merging.

uint64_t patienceSort(int *p, int len) {
	uint64_t c = 0;	// Comparison counter
	int i, j;		// Loop iterators
	piles stacks;	// Pile object

	// Initialise the piles struct
	pilesInit(&stacks);

	// Process first element
	addToPile(&stacks, 0, p[0]);

	//Loop through the elements from the source starting from the 2nd one
	for (i=1; i<len; i++) {
		// Loop through piles
		for (j=0; j<stacks.len; j++) {
			c++;
			if (p[i] >=stacks.piles[j][stacks.pilesLen[j]-1]) {
				// Found a nice pile, add it in
				addToPile(&stacks, j, p[i]);
				break;
			}

			if (j == stacks.len-1) {
				// No available pile. Creating new pile...
				addToPile(&stacks, j+1, p[i]);
				break;
			}
		}
	}

	c += mergePilesToArray(&stacks, p);

	// Free the piles
	clearPile(&stacks);

	printv("Patience sort checks: %i\n", c);
	return c;
}

//
// Shell Sort
//

// Splits the array by generating sub-lists with gaps in between
// Then performs insertion sort on the sublists, shortening the gap each time
// Time complexity: O((n^2)/6). Works about the same (or worse) as Insertion Sort for smaller lists

uint64_t shellSort(int *p, int len) {
	int exp = (int)log2((double)len); // Exponent
	int i, j, subListSize, loops;
	int *subList;
	uint64_t c = 0;
	int gap;
	// Loop through all positive exponents
	while (exp > 0) {
		// Calculate the gap as 2^i
		gap = (int)pow(2,(double)exp--)-1;
		if (gap == 1) {
			c += insertionSort(p, len);
			break;
		}

		// Allocate memory for a sublist
		subListSize = len % gap ? len / gap +1 : len/gap;
		subList = malloc(subListSize * sizeof(int));

		// Loop through gap shifts
		loops = len/gap + len%gap;
		for (i=0; i<loops && subListSize>1; i++) {
			// Test for sublists that may go over len
			if (gap * (subListSize-1) + i >= len && gap != 1) {
				subListSize -= 1;
				if (subListSize == 1){
					break;
				}
			}

			// Generate sublist
			for (j=0; j<subListSize; j++) {
				subList[j] = p[i+j*gap];
			}

			// Sort the sublist
			c += insertionSort(subList, subListSize);

			// Reinsert the sublist
			for (j=0; j<subListSize; j++) {
				p[i+j*gap] = subList[j];
			}
		}
		// Free sublist memory
		free(subList);
	}

	printv("Shell sort checks: %i\n", c);
	return c;
}

//
// Splay Sort
//

// Similar to a tree sort, but inserts new items on the top instead of the bottom
// Useful for lists with many repeating items since common/recent keys will be closer to the top
// Time complexity: O(nlog(n)+n): nlogn for the tree build, an extra n for tree traversal.
uint64_t splaySort(int *p, int len) {
	uint64_t c = 0;
	node *root;
	int i;

	// Set the first node
	root = newNode(p[0]);
	
	for (i=1; i<len; i++) {
		root = splayInsert(root, p[i], &c);
	}

	// Traverse the tree inOrder
	c += treeInOrder(root, p, 0);

	// Clear tree from memory
	treeDelete(root, 0);

	printv("Splay sort checks: %i\n", c);
	return c;
}

//
// Tree Sort
//

// Generates a binary search tree from the input array,
// and then does an inOrder traversal to overwrite the array
// Time complexity is O(nlogn + n), but uses a lot of memory to store the tree
// Most of the functions used here can be found in the data-structures submodule

uint64_t treeSort(int *p, int len) {
	uint64_t c = 0;
	int i;
	node *root;

	// Set the first node
	root = (node*)malloc(sizeof(node));
	root->val = p[0];
	root->left = root->right = NULL;

	// Create the Binary Search Tree
	for (i=1; i<len; i++) {
		//printf("Inserting %i...\n",p[i]);
		c += bstInsert(root, p, i);
	}

	// Traverse the BST in order
	c += treeInOrder(root, p, 0);

	// Clear tree from memory
	treeDelete(root, 0);

	printv("Tree sort checks: %i\n", c);
	return c;
}
