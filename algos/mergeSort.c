/* ====================================================================================
	 __  __                     ____             _         
	|  \/  | ___ _ __ __ _  ___/ ___|  ___  _ __| |_   ___ 
	| |\/| |/ _ | '__/ _` |/ _ \___ \ / _ \| '__| __| / __|
	| |  | |  __| | | (_| |  __/___) | (_) | |  | |_ | (__ 
	|_|  |_|\___|_|  \__, |\___|____/ \___/|_|   \__(_\___|
	                 |___/                                 

	These are sorts that are based off of the merging process.

 ==================================================================================== */

#include "common.h"
#include "mergeSort.h"

#include <stdint.h>
#include <stdlib.h>

//
// In-place Merge Sort
//

// Similar to merge sort, but first finds pre-sorted ascending sub-arrays
// Sorts in-place, and only uses a little bit of auxiliary memory to hold sub-array pointers
// Time complexity O(0.9nlog(n)), but can deviate greatly depending on the input array

uint64_t inPlaceMergeSort (int* p, int len) {
	uint64_t c = 0;		// Comparison counter
	int* subIndex;		// Array of indices that denote the start of each list
	int subIndexLen; 	// Length of the above array
	int i, j;			// Loop iterators
	int beg, mid, end;	// Index pointers
	int temp;			// Temp value for the swap

	// Init the subIndex array
	subIndex = malloc(sizeof(int));
	subIndex[0] = 0;
	subIndexLen = 1;

	// Generate an array of indices where each ascending section begins
	for (i=0; i<len; i++) {
		// Check to see if this item is lower than the previous one
		if (i && p[i] < p[i-1]) {
			// This line is causing a sigtrap...
			//Best to just malloc the whole length of the array for now
			subIndex = (int*)realloc(subIndex, (subIndexLen+1) * sizeof(int));
			subIndex[subIndexLen] = i;
			subIndexLen++;
		}
		c++;
	}

	while (subIndexLen > 1){
		// Loop through each pair for merging
		for (i=0; i<subIndexLen-1; i+=2) {
			beg = subIndex[i];
			mid = subIndex[i+1];
			end = (i+2 >= subIndexLen) ? len : subIndex[i+2];
			printv("\nBeg: %i, Mid: %i, End: %i\n", beg, mid, end);
			// Compare the first element of each list
			while (beg != mid && mid != end) {
				if (p[beg] <= p[mid]) {
					// No need to move anything, just increase the pointer
					beg++;
				} else {
					// Store the value to move as a temp value
					temp = p[mid];
					// Shift all other values up
					for (j=mid-1; j>=beg; j--) {
						p[j+1] = p[j];
					}
					// Place it back at the start
					p[beg] = temp;
					// Increase index pointers
					beg++;
					mid++;
				}
				c++;
			}
			printv("%i <- %i\n",subIndex[i/2], subIndex[i]);
			// Start compacting the subIndex
			subIndex[i/2] = subIndex[i];
		}
		
		// Finish compacting the subIndex
		if (i < subIndexLen) {
			printv("%i <- %i later\n",subIndex[i/2+1], subIndex[subIndexLen-1]);
			subIndex[i/2] = subIndex[subIndexLen-1];
		}
		subIndexLen = (subIndexLen/2) + (subIndexLen%2);
	}

	free(subIndex);
	printv("In-place merge sort checks: %i\n", c);
	return c;
}


//
// Merge Sort
//

// Splits the list in half and sorts them by merging them as two priority queues.
// To get this done, it sorts each priority queue using mergeSort
// Time complexity O(nlog(n)-n), a bit faster than quickSort.

uint64_t mergeSort(int *p, int len) {
	uint64_t c = 0;		// Comparison counter
	int j;				// Loop iterator
	int l, r;			// Index for left/right arrays
	int* t;				// Temporary 'shelf' array
	int i;

	if (len <= 1) {
		return c;
	}

	//printf("Left half:");
	//printList(p, len/2);
	c += mergeSort(p, len/2);

	//printf("Right half:");
	//printList(&p[len/2], len-(len/2));
	c += mergeSort(&p[len/2], len-(len/2));

	// Copy the array
	t = malloc(len * sizeof(int));
	for (j=0; j<len; j++) {
		t[j] = p[j];
	}

	// Loop through both halves and merge them
	l = 0;		// Left index to zero
	r = len/2;	// Right index to halfway
	i = 0;
	while((l < len/2) && (r < len)) {
		i++;
		//printf("L %i(%i) < R %i(%i)\n", l, p[l], r, p[r]);
		// Set the next item in the array to the lowest of the next two values
		p[l+(r-len/2)-1] = t[l] < t[r] ? t[l++] : t[r++];
		c++;
	}
	
	// Fill with the rest of the list, if needed
	//printf("L %i(%i), R %i(%i), Len: %i\n", l, p[l], r, p[r], len);
	for (j = (l<len/2)?l:r; j < ((l<len/2)?len/2:len); j++, i++) {
		//printf("I: %i, J: %i\n", i, j);
		p[i] = t[j];
	}
	//printList(p, len);
	free(t);
	printv("Merge sort checks: %i\n", c);
	return c;
}

// Polyphase merge sort
// Oscillating merge sort
// Cascade merge sort

// I'm skipping these sorting algorithms since they are basically just merge sort but 
// the only difference is the order in which it merges different files together.