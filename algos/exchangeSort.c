/* ====================================================================================
	 _____          _                            ____             _
	| ______  _____| |__   __ _ _ __   __ _  ___/ ___|  ___  _ __| |_   ___
	|  _| \ \/ / __| '_ \ / _` | '_ \ / _` |/ _ \___ \ / _ \| '__| __| / __|
	| |___ >  | (__| | | | (_| | | | | (_| |  __/___) | (_) | |  | |_ | (__
	|_____/_/\_\___|_| |_|\__,_|_| |_|\__, |\___|____/ \___/|_|   \__(_\___|
	                                  |___/

	Exchange sorts are sorting algorithms that involve swapping values in a list.
	Towards the end, the unsorted portions tend to get smaller and smaller so there is
	some probability that they are already sorted, saving a few counts. We will call
	this "<Perm>" for the sake of simplicity but it is just the simplification of
	(1/1! + 1/2! + 1/3! + ... + 1/n!)

 ==================================================================================== */

#include "exchangeSort.h"
#include "common.h"

#include <stdlib.h>

//
// Bubble Sort
//

// Moves from left to right one by one
// Switches the highest value towards the right
// Completes in O((n^2-n)/2 - <Perm>) time
// (with some chance for having an already-soreted list near the end)

uint64_t bubbleSort(int *p, int len) {
	int i, j;				// Loop iterators
	char sorted = 0;		// Sorted flag
	uint64_t c = 0;			// Comparison counter

	for (i=0; i<len && !sorted; i++){
		sorted = 1;
		for (j=0; j<len-i-1; j++, c++) {
			// Is this one in the wrong position compared to the next one?
			if (p[j] > p[j+1]){
				// Swap them!
				p[j] ^= p[j+1] ^= p[j] ^= p[j+1];
				sorted = 0;
			}
		}
	}
	printv("Bubble sort checks: %i\n", c);
	return c;
}

//
// Comb sort
//

// Compares values with another one x values after, 
// gap decreasing with each iteration
// Time complexity O((n^2)/2 - ((n/2)^2)/2 - <Perm>)

uint64_t combSort(int *p, int len) {
	uint64_t c = 0;		// Comparison Counter
	int i;				// Loop iterator
	char sorted = 0;	// Sorted flag
	float shrink = 1.3;	// Shrink factor
	int gap = len;		// Gap between items

	// Loop through until list is sorted
	while (!sorted) {
		// Decrease gap by a bit each time
		gap = (int)(gap/shrink);
		if (gap <= 1) {
			sorted = 1;
			gap = 1;
		}

		// Compare all items with $gap distance between them
		for (i=0; i<len-gap; i++, c++) {
			//Compare and swap
			if (p[i] > p[i+gap]) {
				p[i] ^= p[i+gap] ^= p[i] ^= p[i+gap];
				sorted = 0;
			}
		}
	}
	printv("Comb sort checks: %i\n", c);
	return c;
}

//
// Cocktail Sort
//

// Moves from left to right one by one and then right to left
// Moving the highest value rightward and the lowest value leftward.
// Completes in O((n^2-n)/2) time, the same as Bubble Sort

uint64_t cocktailSort(int *p, int len) {
	int i, j;
	uint64_t c = 0;
	for (i=0; i<len/2; i++){
		// Bubblesort rightward
		for (j=i; j<len-i-1; j++, c++) {
			if (p[j] > p[j+1]){
				// Swap them!
				p[j] ^= p[j+1] ^= p[j] ^= p[j+1];
			}
		}
		// Bubblesort leftward
		for (j=len-i-2; j>i; j--, c++) {
			if (p[j-1] > p[j]){
				// Swap them!
				p[j-1] ^= p[j] ^= p[j-1] ^= p[j];
			}
		}
	}
	printv("Cocktail sort checks: %i\n", c);
	return c;
}

//
// Proportional Exchange Sort
//

// Gets a sorted section of the array and uses that to find a 'median' with which to better split the array in half
// Basically quicksort, but consistently adds on more items after getting an accurate median value
// This version is quite inefficient since it has to re-sort already sorted portions with small unsorted chunks.
// Time complexity for this version: Approx O((n^2)/2). It's not an even deistribution, it has peaks and valleys
// Sample call pexSortRecursive(plist, listLen, 0);

uint64_t pexSortRecursive(int *p, int len, int level) {
	uint64_t c = 0;
	int median, medianVal;
	int sampleSize;
	int i, pr;
	int chunkSize = len / PEX_SIZE;
	// Sort an initial section
	c += quickSortRecursive(p, chunkSize, 1, 0);
	// Only bother to do the pexSort if we have enough size to make it worthwhile
	if (len >= 20) {
	// Loop through each (p) section
		for (pr=1; pr<=PEX_SIZE; pr++) {
			// Set sample size. This is the size of the array that is pre-sorted.
			sampleSize = chunkSize * pr;

			// Check to see if we need to do a partial chunk
			//printf("Checking conditions... %i, %i<%i?\n",pr,chunkSize * PEX_SIZE,len);
			if (pr == PEX_SIZE && chunkSize * PEX_SIZE < len) {
				// This is a partial chunk
				chunkSize = len - chunkSize * PEX_SIZE;
			} else if (pr == PEX_SIZE && chunkSize * PEX_SIZE == len) {
				// No partial chunks, just skip it;
				continue;
			}
			//printf("\n%i: SampleSize: %i\n", level, sampleSize);

			// Get the Median "M"
			// This will be the splitting point for our sorted section into Left "L" and right "R"
			median = sampleSize/2;
			medianVal = p[median];
			//printf("Median: %i(%i)\n", median, p[median]);

			// Print current status of sorted section and unsorted chunk
			//printf("Current sorted:\n");printList(p, sampleSize);printf("Next unsorted:\n");printList(&p[sampleSize], chunkSize); printList(p, len);

			// Swap elements after the median with the end of the unsorted section "U".
			for (i=sampleSize+chunkSize-1; i>median+chunkSize; i--) {
				//printf("%i(%i):%i(%i)\n",i,p[i],i-chunkSize,p[i-chunkSize]);
				p[i] ^= p[i-chunkSize] ^= p[i] ^= p[i-chunkSize];
			}
			// Unsorted chunk should now be between two sorted halves "LUR"
			//printf("After sandwiching. Median: %i(%i)\n", median, p[median]);
			//printList(p, len);
			//printf("Sorting the middle - %i(%i) to %i(%i):\n", median, p[median], median+chunkSize, p[median+chunkSize]);

			// Sort the unsorted centre according to the pivot
			c += quickSortRecursive(&p[median], chunkSize+1, level+1, 0);

			// Find the pivot/median "M" again (it was likely moved due to quickSort)
			//printf("Finding the median from %i(%i) to %i(%i)...\n", median, p[median], median+chunkSize-1, p[median+chunkSize-1]);
			for (i=median; i<=median+chunkSize; i++, c++) {
				if (p[i] == medianVal) {
					median = i;
					//printf("Found the median: %i(%i)\n", median, p[median]);
					break;
				}
			}
			//You should now have an array that looks like "L(UL)M(UR)R"
			//printList(p, len);

			// Sort the left-hand side "L(UL)M" recursively
			//printf("%i: Left recursion from %i(%i) to %i(%i)...\n", level, 0, p[0], median, p[median]);
			c += pexSortRecursive(p,median,level+1);

			// Sort the right hand side  "(UR)R" recurseively 
			//printf("%i: Right recursion from %i(%i) to %i(%i)...\n", level,median+1,p[median+1],sampleSize+chunkSize-1,p[sampleSize+chunkSize-1]);
			c += pexSortRecursive(&p[median+1],sampleSize+chunkSize-median-1,level+1);
		}
	} else {
		// Don't bother. Just quicksort it.
		c += quickSortRecursive(p, len, 1, 0);
	}

	// Finish the function.
	if (level == 0) {
		printv("Proportional Exchange sort checks: %i\n", c);
	}
	return c;
}

// Helper function for pexSort
uint64_t pexSort(int *p, int len) {
	return pexSortRecursive(p, len, 0);
}

//
// Quick Sort
//

// Picks a pivot at random,
// then puts all lower items to the left and all larger ones to the right
// This makes the pivot be in the correct position
// Then recursively sorts the left and right sections
// Default use: quickSort(plist, listLen, 0, (rand()%listLen))
// Time complexity O(nlogn)

uint64_t quickSortRecursive(int *p, int len, int level, int pivot) {
	uint64_t c; 	// The amount of comparisons made
	c = 0;
	int i;		// Left side counter
	int r=0;	// Right (reverse) counters
	//printf("Pivot: %i(%i), Len: %i\n", pivot, p[pivot], len);
	// Start going through the array upwards
	for (i=0; i<len-r;i++) {
		if (i < pivot) {
			// Picked from the left of the pivot
			if (p[i] < p[pivot]) {
				// It's in the correct position, do nothing
			} else if (p[i] > p[pivot]) {
				// It needs to be shifted to the right
				p[i] ^= p[len-1-r] ^= p[i] ^= p[len-1-r];
				// If we're swapping with the pivot, remember to change the pivot's location
				if (len-1-r == pivot) {
					pivot = i;
				}
				r++;
				i--;
			}
		} else if (i == pivot) {
			// Is the pivot, do nothing
			continue;
		} else {
			// Picked from the right
			if (p[i] > p[pivot]) {
				// It's in the correct position, do nothing
			} else if (p[i] < p[pivot]) {
				// It needs to be shifted to the left, just before the pivot
				// printf("%i(%i): This needs to be moved Left to %i(%i)\n",i,p[i],pivot,p[pivot]);
				if (i > pivot+1) {
					// Swap with the one right after the pivot if it is too far away
					p[i] ^= p[pivot+1] ^= p[i] ^= p[pivot+1];
				}
				// It should be right after the pivot now, swap with the pivot
				p[pivot] ^= p[pivot+1] ^= p[pivot] ^= p[pivot+1];
				c++;
				pivot++;
				//printf("Pivot moved to: %i(%i)\n", pivot, p[pivot]);
			}
		}
		c++;
	}
	// Call recursively
	// Left side
	if (pivot > 1) {
		c += quickSortRecursive(p, pivot, level+1, rand() % pivot);
	}
	// Right side
	if (len-pivot-1 > 1) {
		c += quickSortRecursive(&p[pivot+1], len-pivot-1, level+1, rand() % (len-pivot-1));
	}
	if (level == 0) {
		printv("Quick sort checks: %i\n", c);
	}
	return c;
}

// Helper function
uint64_t quickSort(int *p, int len) {
	return quickSortRecursive(p, len, 0, (rand()%len));
}


//
// Slow Sort
//

// Split the array into halves, then pull the higher one out
// Basically, it has to check the whole array recursively before sorting another item
// Time complexity is O(n^(log2(n)/2). It doesn't match perfectly, but it's along that order.

uint64_t slowSortRecursive(int *p, int start, int end, int l) {
	int mid;				// The midpoint of the array
	uint64_t c = 0; 	// The amount of comparisons made
	// Error checking (don't do anything if the length of array is 1)
	if (end <= start || end-start < 1) {
		return 0;
	}
	// Split the array in half
	mid = (start + end) / 2;
	c += slowSortRecursive(p, start, mid, l+1);
	c += slowSortRecursive(p, mid+1, end, l+1);

	// Do the check
	c++;
	if (p[end] < p[mid]) {
		p[end] ^= p[mid] ^= p[end] ^= p[mid];
	}

	// Do it again, without the final item in the list
	c += slowSortRecursive(p, start, end-1, l+1);

	return c;
}

uint64_t slowSort(int *p, int len) {
	uint64_t c = 0;			// Comparison counter
	c += slowSortRecursive(p, 0, len-1, 0);
	printv("Slow sort checks: %i\n", c);
	return c;
}
//
// Stooge Sort
//

// Recursively splits the list into groups of three
// Then sorts the left two, right two then left two again
// Completes in O(n^(log2(3)/log2(1.5))) time. However, this function's speed flatlines every few numbers,
// causing n=7,8,9 to have the exact same speed until a large spike at 10 and another flatline for 10,11,12,13

uint64_t stoogeSortRecursive(int *p, int len, int l) {
	int chunk; 
	uint64_t c = 1;
	// If length is greater than 2, reduce array width and do another Stooge Sort
	if (len > 2) {
		// Reduce length of array to a shorter one with this formula
		chunk = (len / 3) * 2 + (len % 3);
		// Recursively invoke Stooge Sort for the 1st 2/3rds, then the last 2/3rds then the 1st 2/3rds again
		c += stoogeSortRecursive(p, chunk, l+1);
		c += stoogeSortRecursive(&p[len/3], chunk, l+1);
		c += stoogeSortRecursive(p, chunk, l+1);
	} else {
		// Check if they need swapping again
		if (p[0] > p[len-1]){
			p[0] ^= p[len-1] ^= p[0] ^= p[len-1];
		}
	}
	// Print only if we are in the root recursion
	return c;
}

uint64_t stoogeSort(int *p, int len) {
	uint64_t c = 0;			// Comparison counter
	c += stoogeSortRecursive(p, len, 0);
	printv("Stooge sort checks: %i\n", c);
	return c;
}