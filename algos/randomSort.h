#ifndef _RANDOMSORT_H_
#define _RANDOMSORT_H_

#include <stdint.h>

// Bogo Sort function
uint64_t bogoSort(int *p, int len);

// Bozo Sort function
uint64_t smartBozoSort(int *p, int len);
uint64_t dumbBozoSort(int *p, int len);

// Brute Sort function
uint64_t bruteForceSort(int *p, int len);

// Goro Sort function
uint64_t goroSort(int *p, int len);

#endif // _RANDOMSORT_H_