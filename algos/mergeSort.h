#ifndef _MERGESORT_H_
#define _MERGESORT_H_

#include <stdint.h>

// Merge Sort function
uint64_t mergeSort(int *p, int len);

// In-place Merge Sort Function
uint64_t inPlaceMergeSort(int *p, int len);

#endif // _MERGESORT_H_