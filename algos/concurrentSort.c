/* ====================================================================================
	  ____                                           _   ____             _
	 / ___|___  _ __   ___ _   _ _ __ _ __ ___ _ __ | |_/ ___|  ___  _ __| |_   ___
	| |   / _ \| '_ \ / __| | | | '__| '__/ _ | '_ \| __\___ \ / _ \| '__| __| / __|
	| |__| (_) | | | | (__| |_| | |  | | |  __| | | | |_ ___) | (_) | |  | |_ | (__
	 \____\___/|_| |_|\___|\__,_|_|  |_|  \___|_| |_|\__|____/ \___/|_|   \__(_\___|

	These are sorts that can use threading to sort differentparts of the array in
	parallel using a combination of different threads. These functions don't use
	threading so that they can be run on simpler hardware.

 ==================================================================================== */

#include "concurrentSort.h"
#include "insertionSort.h"
#include "mergeSort.h"
#include "common.h"

#include <math.h>
#include <stdlib.h>
#include <pthread.h>

//
// Sublist splitter
//

// Some of the functions here only work on arrays of size 2^n size
// This function is used to split it into useful chunks of the correct size and then merge them together
// Remember that one of the inputs is a function pointer

// Proper splitter function
uint64_t sublistSplitter(int *p, int len, uint64_t(*func)(int*, int)) {
	uint64_t c = 0;			// Comparison counter
	int subListStart = 0;	// Start of the next sublist
	int subListLen;

	// Split into sublists
	while (subListStart < len) {
		subListLen = pow(2, (int)log2(len - subListStart));
		c += (*func)(&p[subListStart], subListLen);
		subListStart += subListLen;
	}

	// Sort the almost-sorted list using inPlaceMergeSort()
	c += inPlaceMergeSort(p, len);

	// Return the count
	return c;
}

//
// Batcher's odd-even mergesort
//

// Combines the odd-even transposition sort and merge sort algorithms to sort an array of elements
// I have no idea what that means, but it's supposed to be great for parallel processing.
// Time complexity O(n^2)

uint64_t batcherSort(int *p, int len) {
	uint64_t c = 0;			// Comparison counter
	int i, j, k, l;			// Loop iterators

	// Double until you get to the max value
	for (i=1; i<len; i*=2) {
		// Get every power of 2 below that
		for (j=i; j>0; j/=2) {
			// Loop through the keys between J and Len-J
			for (k=j%i; k<=len-j-1; k+=1) {
				// Loop through each item in the list until its limit depending on the size of J and K
				for(l=0; l<=(j-1 > len-k-j-1 ? len-k-j-1 : j-1); l++) {
					// I think this checks if j is small. I'm not entirely sure.
					if ((l+k)/(i*2) == (l+k+j)/(i*2)) {
						// Compare the two items before swapping
						if (p[l+k] > p[l+k+j]) {
							// Swap them
							p[l+k] ^= p[l+k+j] ^= p[l+k] ^= p[l+k+j];
						}
						c++;
					}
				}
			}
		}
	}

	printv("Batcher Odd-even Merge sort checks: %i\n", c);
	return c;
}

//
// Bitonic Sort
//

// Sorts items by sorting sub-arrays in bitonic arrays which are sub-lists that ascend and then descend.
// Bitonic sort only works for lengths that are a power of 2, so they are sorted bitonically then with In-place Merge Sort
// Time complexity is pretty complicated, but it can approximate to O(17n)... ish

// Sorts an array using Bitonic Sort
// Only works for lengths which are a power of 2
uint64_t bitonicSortSublist(int *p, int len) {
	uint64_t c = 0;		// Comparison counter
	int i, j, k;		// Loop iterators
	int l;

	// Double until you reach the length limit to get the max bitonic array size
	for (i=2; i<=len; i*=2) {
		// Half until you reach zero to get the size of the bitonic comparisons length
		for (j=i/2; j>0; j/=2) {
			// Loop through each item in the array as normal
			for (k=0; k<len; k++) {
				// Calculate its corresponding number for this particular bitonic comparison
				// J will always be a power of 2, so it will keep all the 
				l = k ^ j;
				// Make sure that L is the one further down the list to avoid duplicate comparisons
				if (l > k) {
					// Check whether this bitonic array is sorting upwards or downwards
					// Then swap the two items in the right direction
					if ((((i & k) == 0) && (p[k] > p[l])) ||
						(((i & k) != 0) && (p[k] < p[l]))) {
						// Swap them
						p[k] ^= p[l] ^= p[k] ^= p[l];
					}
					c++;
				}
			}
		}
	}

	return c;
}

// Parent function
uint64_t bitonicSort(int *p, int len) {
	uint64_t c = 0;			// Comparison counter

	// Execute sublist splitter
	c += sublistSplitter(p, len, &bitonicSortSublist);

	printv("Bitonic sort checks: %i\n", c);
	return c;
}

//
// Odd-Even Sort
//

// Compares odd/even pairs and switches them if out of order.
// Fairly similar to Bubble Sort, but can be optimised to run with multiple processors
// Time complexity is the same as Bubble Sort: O((n^2-n)/2 - <Perm>)

// just a struct to help pass parameters into our threads
struct oddEvenParams {
	int *p;
	int len;
	int start;
};
char sorted;
// Threading function
void *oddEvenThread(void *vargp) {
	uint64_t c = 0;
	struct oddEvenParams *params = vargp;
	int *p = params->p;
	for (int i = params->start; i < params->len - 1; i += 2, c++) {
		if (p[i] > p[i + 1]) {
			p[i+1] ^= p[i] ^= p[i+1] ^= p[i];
			sorted = 0;
		}
	}
	return (void *) c;
}

// Main oddEvenSort function
uint64_t oddEvenSort(int *p, int len) {
	uint64_t c = 0;/*
	// Set the parameters to initialise the threads
	struct oddEvenParams oddParams = {p,len,1}, evenParams = {p,len,0};
	// Thread types IDs
	pthread_t oddThread, evenThread;
	// Return values for the threads
	void *oddReturn, *evenReturn;
	sorted = 0;
	while (!sorted) {
		sorted = 1;
		// Create the two threads
		pthread_create(&oddThread, NULL, oddEvenThread, (void *)&oddParams);
		pthread_create(&evenThread, NULL, oddEvenThread, (void *)&evenParams);
		// Wait for threads to complete
		pthread_join(oddThread, &oddReturn);
		pthread_join(evenThread, &evenReturn);
		// Add return values
		c += (uint64_t)oddReturn + (uint64_t)evenReturn;
	}*/
	printv("Odd-Even Sort checks: %i\n", c);
	return c;
}

//
// Pairwise sorting network
//

// Pretty much the same as Batcher's odd-even mergesort, but with a different order of operations
// It also removes one nested loop so it's slightly faster
// Time complexity O(0.8*n^2)

uint64_t pairwiseSort(int *p, int len) {
	uint64_t c = 0;			// Comparison counter
	int i, j, k;			// Loop iterators

	// Start large and continue smaller till you reach 1
	for (i=len/2; i>0; i/=2) {
		// Then go from there until Len
		for (j=i; j<len; j++) {
			// Then increase one by one at a time
			for (k=j-i; k>=0; k-=i) {
				// Compare the two items before swapping
				if (p[k+i] < p[k]) {
					// Swap them
					p[k+i] ^= p[k] ^= p[k+i] ^= p[k];
				}
				c++;
			}
		}
	}

	printv("Pairwise sorting network checks: %i\n", c);
	return c;
}

//
// Sample Sort
//

// Like quicksort, but uses multiple pivots instead of just one.
// Uses a loop instead of recursion to make it print a bit more nicely
// Time complexity O(n*log(n) - n/2)

uint64_t sampleSort(int *p, int len) {
	uint64_t c = 0;			// Comparison counter
	int* pivots;			// Array of pivots
	int i, k;				// Loop iterator
	int temp, pivot;		// Temporary value for swaps
	int l, r, m;			// Used for binary search in sample indices
	int limit;

	// Just do insertionSort if the list is too short
	if (len <= 1) {
		printv("Length %i, skipping...\n", len);
		return c;
	} else if (len < SAMPLESORT_DIVISOR*2) {
		printv("Sorting %i items using Insertion Sort...\n", len);
		c += insertionSort(p, len);
	} else {
		// List is long enough. Let's do a nice Sample Sort
		// Get a set of pivots
		pivots = malloc(len/SAMPLESORT_DIVISOR * sizeof(int));
		for (i=0; i<len/SAMPLESORT_DIVISOR; i++) {
			// Get a random index
			temp = randInt(len-i, i) + i;
			// Swap them if they're different numbers
			if (temp != i) {
				p[i] ^= p[temp] ^= p[i] ^= p[temp];
			}
			// Set the pivot index pointer
			pivots[i] = i;
			c++;
		}

		// Use Sample Sort to sort the sample indices
		c += sampleSort(p, len/SAMPLESORT_DIVISOR);

		// Sort it into buckets
		for (i=len/4; i<len; i++) {
			temp = p[i];
			// Search for the correct position

			// Get the closest index
			l = 0;
			r = len/SAMPLESORT_DIVISOR-1;
			while (l <= r) {
				c++;
				// Get the mid index
				m = (l + r) / 2;
				if (p[pivots[m]] < p[i]) {
					l = m + 1;
				} else if (p[pivots[m]] > p[i]) {
					r = m - 1;
				} else {
					break;
				}
			}

			// Store a few values for later
			pivot = p[pivots[m]];									// Value for the pivot (not its index)
			limit = p[i]>p[pivots[m]] ? pivots[m]+1 : pivots[m];	// Things will shift, so we need this to stay the same

			// Put it into position and shift all other elements rightwards
			for (k=i-1; k>=limit; k--) {
				p[k+1] = p[k];
			}
			// Do the last one
			p[k+1] = temp;

			// Shift the index pointers over
			for (k=(temp<pivot ? m : m+1); k<len/SAMPLESORT_DIVISOR; k++) {
				pivots[k]++;
			}
		}
		// Do Sample Sort recursively on remaining items
		temp = 0;
		for (i=0; i<len/SAMPLESORT_DIVISOR; i++) {
			c += sampleSort(&p[temp], pivots[i]-temp);
			temp = pivots[i]+1;
		}
		// Do it one last time for the gap between the last pivot and the end of the array
		c += sampleSort(&p[temp], len-temp);

		printv("Sample Sort checks: %i\n", c);
		free(pivots);
	}

	return c;
}