#ifndef _EXCHANGESORT_H_
#define _EXCHANGESORT_H_

#include <stdint.h>

#define PEX_SIZE 4

// Bubble Sort function
uint64_t bubbleSort(int *p, int len);

// Comb Sort function
uint64_t combSort(int *p, int len);

// Cocktail Sort function
uint64_t cocktailSort(int *p, int len);

// Proportional Exchange Sort
uint64_t pexSort(int *p, int len);
uint64_t pexSortRecursive(int *p, int len, int level);

// Quick Sort
uint64_t quickSort(int *p, int len);
uint64_t quickSortRecursive(int *p, int len, int level, int pivot);

// Slow Sort function
uint64_t slowSort(int *p, int len);

// Stooge Sort function
uint64_t stoogeSort(int *p, int len);

#endif // _EXCHANGESORT_H_