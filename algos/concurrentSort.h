#ifndef _CONCURRENTSORT_H_
#define _CONCURRENTSORT_H_

#include <stdint.h>

#define SAMPLESORT_DIVISOR 4

// Batcher odd–even mergesort function
uint64_t batcherSort(int *p, int len);

// Bitonic Sort function
uint64_t bitonicSort(int *p, int len);

// Odd-Even Sort function
uint64_t oddEvenSort(int *p, int len);

// Pairwise Sort function
uint64_t pairwiseSort(int *p, int len);

// Sample Sort function
uint64_t sampleSort(int *p, int len);

#endif // _CONCURRENTSORT_H_