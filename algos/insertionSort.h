#ifndef __INSERTIONSORT_H__H_
#define __INSERTIONSORT_H__H_

#include <stdint.h>

// Gnome Sort function
uint64_t gnomeSort(int *p, int len);

// Insertion Sort function
uint64_t insertionSort(int *p, int len);

// Library Sort function
uint64_t librarySort(int *p, int len);

// Patience Sort function
uint64_t patienceSort(int *p, int len);

// Shell Sort function
uint64_t shellSort(int *p, int len);

// Splay Sort function
uint64_t splaySort(int *p, int len);

// Tree Sort function
uint64_t treeSort(int *p, int len);

#endif // __INSERTIONSORT_H__H_