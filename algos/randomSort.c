/* ====================================================================================
	 ____                 _                 ____             _         
	|  _ \ __ _ _ __   __| | ___  _ __ ___ / ___|  ___  _ __| |_   ___ 
	| |_) / _` | '_ \ / _` |/ _ \| '_ ` _ \\___ \ / _ \| '__| __| / __|
	|  _ | (_| | | | | (_| | (_) | | | | | |___) | (_) | |  | |_ | (__ 
	|_| \_\__,_|_| |_|\__,_|\___/|_| |_| |_|____/ \___/|_|   \__(_\___|
                                                                    

	These are sorts that involve some kind of random choices. This makes many of them
	not have an upper limit for a worst case scenario and generally very ineficcient
	but they are fun for the sake of finding poorly-performing algorithms.

 ==================================================================================== */

#include "algos/exchangeSort.h"
#include "common.h"
#include "data-structures/array.h"
#include "data-structures/binarr.h"
#include "randomSort.h"

#include <string.h>
#include <stdlib.h>

//
// Bogo Sort
//

// Shuffles the array and checks if it sorted
// Completes in n! shuffles on average
// Time complexity: O((n!/1!) + (n!/2!) + (n!/3!) + (n!/4!) ... + (n!/(n-1)!) + (n!/n!))
// It can be simplified to O(x!(e-1))

uint64_t bogoSort(int *p, int len) {
	uint64_t c = 0;		// Comparison counter
	char sorted = 0;	// Boolean flag to check if the array is sorted

	// This is used to track how deep the Bogo check has penetrated the list
	int *a = (int*)calloc(len, sizeof(int));

	// Keep it going while unsorted
	while (!sorted) {
		sorted = 1;
		//Check if the list is sorted
		for (int i=1; i<len; i++, a[i-1]++) {
			c++;
			if (p[i] < p[i-1]){
				// List isn't sorted. Let's shuffle it
				sorted = 0;
				shuffle(p, len, c);
				break;
			}
		}
	}

	printv("Bogo sort checks: %i\n", c);
	free(a);
	return c;
}

//
// Bozo Sort
//

// This sort comes in two different flavours: dumb and smart.
// The dumb method swaps two elements at random and checks if the array is sorted.
// The smart method does the same, but checks if they are out of order before swapping.

// Dumb Bozo Sort has a time complexity of O(n!) for loops and O(n!*e) if you include array checks
// Smart Bozo Sort is a lot more volatile, but it averages out to around O(n^5.5) for loops and O(8*n^5.5) with checks

uint64_t bozoSort(int *p, int len, char smart) {
	uint64_t c = 0;		// Comparison counter
	uint32_t seed = 0;	// Seed for random number generation
	int rand1, rand2;	// Random indices for numbers to compare
	int i=1, j;			// Loop iterators

	while (i) {
		// Pick two random numbers
		rand1 = randInt(len, seed++);
		while (1) {
			rand2 = randInt(len, seed++);
			if (rand1 != rand2) {
				break;
			}
		}

		// Make sure that rand1 is lower (Only in smart mode)
		if (smart && rand1 > rand2) {
			rand1 ^= rand2 ^= rand1 ^= rand2;
		}

		// Swap them if needed (Only in smart mode)
		c++;
		if (!smart || p[rand1] > p[rand2]) {
			p[rand1] ^= p[rand2] ^= p[rand1] ^= p[rand2];
		}
		
		// Check if list is sorted
		for (j=1; j<len; j++) {
			c++;
			if (p[j-1] > p[j]) {
				// List is out of order
				break;
			} else if (j == len-1) {
				// List is in order
				i = 0;
			}
		}
	}

	printv("Bozo sort checks: %i\n", c);
	return c;
}

// Wrapper function for the smart version of Bozo Sort
uint64_t smartBozoSort(int *p, int len) {
	return(bozoSort(p, len, 1));
}

// Wrapper function for the dumb version of Bozo Sort
uint64_t dumbBozoSort(int *p, int len) {
	return(bozoSort(p, len, 0));
}

//
// Brute Force Sort
//

// This algorithm lists all the possible permutations of the array
// And then checks each one to see if they're in order
// Time complexity for comparisons O(0.9*n!) but the amount of permutations is about O(n!/2)

uint64_t bruteSortRecursive(int *p, int len, int index) {
	uint64_t c = 0;		// Comparison counter
	int i;				// Loop iterator
	static char done;	// Checker for whether the array is sorted

	// Set done tag to zero only on the first iteration
	if (!index) {
		done = 0;
	}

	// Have we reached the end of a recursion?
	if (len-1 == index) {
		// Check if the array is sorted
		for (i=1; i<len; i++) {
			c++;
			if (p[i] < p[i-1]) {
				return (c);
			}
		}
		// List is sorted! Set flag to exit the function
		done++;
		return (c);
	}
	for (i=index; i<len; i++) {
		// Swap the first element with the next
		if (index != i){
			p[index] ^= p[i] ^= p[index] ^= p[i];
		}
		
		// Perform recursion
		c += bruteSortRecursive(p, len, index+1);
		if (done) {
			return c;
		}

		// Swap back to next item
		if (index != i) {
			p[index] ^= p[i] ^= p[index] ^= p[i];
		}

	}
	return c;
}

// Helper function
uint64_t bruteForceSort(int *p, int len) {
	int c =  bruteSortRecursive(p, len, 0);
	printv("Brute sort checks: %i\n", c);
	return c;
}

//
// Goro Sort
//

// Goro has four arms. He uses two hands to hold correct items with his infinite fingers.
// He then bangs the table, randomly shiffling the remaining unheld items.
// The process repeats until the list is sorted.
// Time complexity O(2*n^2)

uint64_t goroSort(int *p, int len) {
	uint64_t c = 0;		// Comparison counter
	int psorted[len];	// Pre-sorted array
	int bang[len];		// Array to hold items to be shuffled
	int bangLen;		// Length of bang array
	binarr fingers;		// Binary array to show which items are being held
	int i, j;			// Loop iterators
	int seed = 0;		// Seed for randomiser
	char loop = 1;		// Loop checker

	// Get a sorted list using quicksort
	memcpy(psorted, p, len * sizeof(int));
	quickSort(psorted, len);
	
	// Initialise binary array
	binarrInit(&fingers, len);

	// Loop until we have a sorted array
	while (loop) {
		for (i=0, bangLen=0; i<len; i++, c++) {
			if (p[i] == psorted[i]) {
				binarrSet (&fingers, i, 1);
			} else {
				bangLen++;
			}
		}

		// Populate the auxiliary array
		for (i=0, j=0; i<len; i++) {
			if (!binarrGet(&fingers, i)) {
				bang[j++] = p[i];
			}
		}

		// Shuffle the auxiliary array
		shuffle(bang, bangLen, seed++);

		// Insert it back in
		for (i=0, j=0; i<len; i++, c++) {
			if (!binarrGet(&fingers, i)) {
				p[i] = bang[j++];
			}
		}

		// Check if it's sorted
		loop = 0;
		for (i=1; i<len; i++) {
			c++;
			if (p[i-1] > p[i]) {
				// List is out of order
				loop = 1;
				break;
			}
		}
	}

	binarrFree(&fingers);
	printv("Goro sort checks: %i\n", c);
	return c;
}