#ifndef _SELECTIONSORT_H_
#define _SELECTIONSORT_H_

#include <stdint.h>

#define TOURN_EMPTY 0
#define TOURN_FILLED 1
#define TOURN_LOCKED 2
#define TOURN_SIZE 7

// Bingo Tree Sort function
uint64_t bingoSort(int *p, int len);

// Cartesian Tree Sort function
uint64_t cartesianSort(int *p, int len);

// Cycle Tree Sort function
uint64_t cycleSort(int *p, int len);

// Heap Sort function
uint64_t heapSort(int *p, int len);

// Selection Sort function
uint64_t selectionSort(int *p, int len);

// Smooth Sort function
uint64_t smoothSort(int *p, int len);

// Tournament Sort function
uint64_t tournamentSort(int *p, int len);

// Weak heap Sort function
uint64_t weakHeapSort(int *p, int len);

#endif // _SELECTIONSORT_H_