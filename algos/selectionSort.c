/* ====================================================================================
	 ____       _           _   _             ____             _
	/ ___|  ___| | ___  ___| |_(_) ___  _ __ / ___|  ___  _ __| |_   ___
	\___ \ / _ | |/ _ \/ __| __| |/ _ \| '_ \\___ \ / _ \| '__| __| / __|
	 ___) |  __| |  __| (__| |_| | (_) | | | |___) | (_) | |  | |_ | (__
	|____/ \___|_|\___|\___|\__|_|\___/|_| |_|____/ \___/|_|   \__(_\___|

	These are algorithms that sort by isolating one element in the list and then placing
	it at the beginning or end of the array.

 ==================================================================================== */

#include "selectionSort.h"
#include "data-structures/binarr.h"
#include "data-structures/piles.h"
#include "data-structures/tree.h"
#include "common.h"

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

//
// Bingo sort
//

// This is similar to Selection Sort, but it does another pass after each new unique element
// The second pass puts all equal items to their correct position on the array
// Good for arrays with a lot of duplicate items
// Time complexity O((n^2-n)/2 - k), where k is the time saved with duplicate items.

uint64_t bingoSort(int *p, int len) {
	uint64_t c = 0;	// Comparison counter
	int i, j;									// Loop iterators
	int low = p[0];								// Lowest item in the list
	int* lowQueue = malloc(len * sizeof(int));	// Array to hold repeated items
	int lowQueueSize;							// Length variable

	// Loop through positions
	for (i=0; i<len; i) {
		lowQueueSize = 0;
		// Reset lowest item
		low = i;
		// Loop through the rest of the items to find the lowest
		for (j=i+1; j<len; j++) {
			c++;
			if (p[j] < p[low]) {
				// Replace with new lower value and add to queue
				low = j;
				lowQueue[0] = j;
				lowQueueSize = 1;
			} else if (p[j] == p[low]) {
				// Add this to the queue
				lowQueue[lowQueueSize++] = j;
			}
		}
		printv("Low: %i(%i)\n", low, p[low]);
		//printList(lowQueue, lowQueueSize);

		// This is for fixing a glitch when we get to the last element
		i += lowQueueSize ? 0 : 1;

		// Loop through all found iterations of this value
		for (j=0; j<lowQueueSize; j++) {
			// Swap only if the item is greater than our current loop position
			if (lowQueue[j] != i ) {
				printv("Swapping %i(%i) with %i(%i)\n", i, p[i], lowQueue[j], p[lowQueue[j]]);
				p[i] ^= p[lowQueue[j]] ^= p[i] ^= p[lowQueue[j]];
			}
			i++;
		}
	}

	free(lowQueue);
	printv("Bingo sort checks: %i\n", c);
	return c;
}

//
// Cartesian Tree sort
//

// Generate a cartesian tree structure from the array
// Then disassemble it from the root downwards into a priority queue
// Sort the queue using selection sort, then add the popped item's children to the queue
// Time complexity O(nlog(n)/2+(n^2)/6)

void printCart (node* n, int level) {
	if (n == NULL) {
		return;
	}
	for (int i=0;i<level;i++) {
		printf(" ");
	}
	printf("%i\n", n->val);
	printCart(n->left, level+1);
	printCart(n->right, level+1);
	return;
}
uint64_t cartesianSort(int *p, int len) {
	int i, j, k, low;	// Iterators
	node* nextNode;		// The next node to insert into the tree
	node* curNode;		// The node in the tree that is currently being tested
	node* prevNode;		// Parent node of curNode
	node* root = NULL;	// Root of the Cartesian tree
	node* t;			// Temp node for swapping
	node** queue;		// Pritority queue, an array of de-treed nodes
	int queueLen;		// Length of priority queue
	uint64_t c = 0;		// Check counter

	// Construct cartesian tree
	for (i=0; i<len; i++) {
		//printf("\nProcessing %i(%i)\n", i, p[i]);
		nextNode = malloc(sizeof(node));
		nextNode->val = p[i];
		nextNode->left = nextNode->right = NULL;

		// Place it in the tree
		if (i) {
			// This is a 2nd+ node, let's find its correct place
			curNode = prevNode = root;
			while (1) {
				// Loop until we get to where we need to (just trying to avoid recursion)
				//printf("Curnode: %i, root: %i\n", curNode->val, root->val);
				c++;
				if (nextNode->val > curNode->val) {
					//printf("Greater than %i, moving right\n",curNode->val);
					// Let's move right
					if (curNode->right == NULL) {
						// Insert into  right
						//printf("Inserting into right child of %i\n",curNode->val);
						curNode->right = nextNode;
						break;
					} else {
						// Continue into the next node
						prevNode = curNode;
						curNode = curNode->right;
					}
				} else {
					//printf("Lesser than %i, making it the left branch of %i...\n",curNode->val,nextNode->val);
					// Let's make this the new root
					if (curNode == root) {
						root = nextNode;
					} else {
						prevNode->right = nextNode;
					}
					
					nextNode->left = curNode;
					break;
				}
			}
		} else {
			// This is the first node, set it to be the root
			root = nextNode;
		}
		
	}
	//printCart(root, 0);
	

	// Create the priority queue
	queue = malloc(len * sizeof(node*));
	queueLen = 1;
	i=0;	// Index of the p array
	queue[0] = root;

	// Loop through all elements in the tree
	// We're not doing this recursively to prevent threading
	while (i < len) {
		//printf("I: %i\n",i);
		// Perform selection sort to get the lowest item
		low = i;
		for (j=i+1; j<queueLen; j++) {
			c++;
			if (queue[j]->val < queue[low]->val) {
				low = j;
			}
		}
		//printf("Lowest item: %i(%i)\n", low, queue[low]->val);
		// Swap if needed
		if (low != i) {
			//printf("Swapping #%i with #%i\n", low, i);
			t = queue[i];
			queue[i] = queue[low];
			queue[low] = t;
		}

		// Add new items to priority queue
		if (queue[i]->left != NULL) {
			queue[queueLen++] = queue[i]->left;
		}
		if (queue[i]->right != NULL) {
			queue[queueLen++] = queue[i]->right;
		}
		i++;

		// Print the priority queue out
		//printf("Priority queue: [");
		for (j=0;j<queueLen;j++) {
			//printf("%i, ", queue[j]->val);
		}
		//printf("]\n");
	}

	// Priority queue is sorted, let's insert it
	for (i=0;i<len;i++) {
		p[i] = queue[i]->val;
	}


	treeDelete(root, 0);
	free(queue);
	printv("Cartesian tree sort checks: %i\n", c);
	return c;
}

//
// Cycle Sort
//

// Put each item in its correct position by counting the items in the array that are lower than itself.
// When a swap is made, continue from the swapped item.
// Time complexity O(1.5*n^2), quite a bit worse than even Bubble Sort.

uint64_t cycleSort(int *p, int len) {
	uint64_t c = 0;	// Comparison counter
	int cycleStart;	// Start of the current cycle
	int current;	// Current element we're trying to position
	int position;	// Number of elements lower than the current one
	int i, j;		// Loop iterators

	// Make each element the start of their own cycle
	for (cycleStart=0; cycleStart<len; cycleStart++) {
		//printf("Starting new cycle at #%i(%i)\n",cycleStart, p[cycleStart]);
		current = p[cycleStart];
		//printf("Current: %i->%i\n",cycleStart, current);
		position = cycleStart;
		// Find its correct position
		for (i=cycleStart+1; i<len; i++) {
			c++;
			if (p[i] < current) {
				position++;
			}
		}
		// Skip if it's in the correct position
		if (cycleStart == position) {
			continue;
		}
		// In case there are any duplicates, just add it towards the end
		//printf("###p[%i]: %i, curr: %i###\n", position, p[position], current);
		while (p[position] == current) {
			c++;
			position++;
		}
		// Ok, let's do the swap
		//printf("Swap %i with %i(%i)\n", current, position, p[position]);
		p[position] ^= current ^= p[position] ^= current;

		// Do the same thing for rest of the cycle
		while (position != cycleStart) {
			//printf("Now looking for where to place %i...\n", current);
			position = cycleStart;
			for (i=cycleStart+1; i<len; i++) {
				c++;
				if (p[i] < current) {
					position++;
				}
			}
			//printf("\tp[%i]: %i, curr: %i\n", position, p[position], current);
			// In case there are any duplicates, just add it towards the end
			while (p[position] == current) {
				c++;
				position++;
			}
			// Ok, let's do the swap
			//printf("Swap %i with %i(%i)\n", current, position, p[position]);
			p[position] ^= current ^= p[position] ^= current;
		}
	}

	printv("Cycle sort checks: %i\n", c);
	return c;
}

//
// Heap Sort
//

// Take the array and heapify it (make it into a heap where each child is lower than its parents)
// Then pop out the top element and sift down to remake the heap
// Time complexity O(nlog(n)).


// Gets the parent of an index 'i'
int parent(int i) {
	return((i-1)/2);
}
// Returns the first of two children
int children(int i) {
	return(i*2+1);
}

// Just a function to print the heap
void printHeap(int *p, int len) {
	int row = 0;
	for(int i=0; i<len; i++) {
		if (i && (int)log2(i+1) != row) {
			row = (int)log2(i+1);
			printf("\n");
		}
		printf("%i, ", p[i]);
	}
	printf("\n");
}

uint64_t siftDown(int *p, int len, int start) {
	uint64_t c = 0; // Comparison counter
	int child = children(start);
	int high;
	while (child < len) {
			if (child == len-1) {
				// Last one, only do one comparison
				c++;
				if (p[child] > p[parent(child)]) {
					//printf("Swapping %i & %i\n",p[child], p[parent(child)]);
					p[child] ^= p[parent(child)] ^= p[child] ^= p[parent(child)];
				}
				break;
			} else {
				// Compare children
				c += 2;
				high = p[child] > p[child+1] ? child : child+1;
				//printf("high: %i\n",p[high]);
				if (p[high] > p[parent(child)]) {
					//printf("Swapping %i & %i\n",p[high], p[parent(child)]);
					p[high] ^= p[parent(child)] ^= p[high] ^= p[parent(child)];
				}
				child = children(high);
			}
		}

	return c;
}

uint64_t heapSort(int *p, int len) {
	uint64_t c = 0;	// Comparison counter
	int child;		// Index of first of two children

	// Heapify the array
	//printHeap(p, len);
	for (int i=parent(len-1); i>=0; i--) {
		child = children(i);
		//printf("Sifting %i -> %i,%i...\n", p[i], p[child], p[child+1]);
		c += siftDown(p, len, i);
	}

	// Sift through to an ordered array
	//printHeap(p, len);
	for (len;len>1;len--) {
		// Swap the top item with the last
		//printList(p, len);
		//printf("Swapping %i with %i\n",p[0],p[len-1]);
		p[0] ^= p[len-1] ^= p[0] ^= p[len-1];
		c += siftDown(p, len-1, 0);
	}

	printv("Heap sort checks: %i\n", c);
	return c;
}

//
// Selection Sort
//

// For each item, move rightward and keep track of the lowest item found
// At the end of each round, swap the leftmost unordered item with the lowest item found
// Time complexity O((n^2-n)/2).

uint64_t selectionSort(int *p, int len) {
	uint64_t c = 0;	// Comparison counter
	int i, j;				// Loop iterators
	int low = p[0];			// Lowest item in the list

	for (i=0;i<len;i++) {
		// Reset lowest known item
		low = i;
		// Loop through the rest of the list
		for (j=i+1;j<len;j++) {
			c++;
			// Compare them
			if (p[j] < p[low]) {
				// Replace with new lower value
				low = j;
			}
		}
		if (low != i) {
			// Swap them
			p[i] ^= p[low] ^= p[i] ^= p[low];
		}
	}

	printv("Selection sort checks: %i\n", c);
	return c;
}

//
// Smooth Sort
//

// Creates a series of ascending Leonardo trees all while sifting leftwards and downwards
// It then dequeues the trees by breaking them up and storing the highest calue to the right
// Time complexity: O(nlog(n)*3) on average, O(n*4) for a close-to-sorted list.

int leonardo(int leo) {
	int i;
	int m2, m1, m0;

	m2 = m1 = 1;

	// Return redundant inputs
	if (leo == 0 || leo == 1) {
		return 1;
	}

	for (i=2; i<=leo; i++) {
		// Perform the sum
		m0 = m1 + m2 + 1;
		// Push the vars leftward
		m2 = m1;
		m1 = m0;
	}
	return m0;
}

void printb(int n) {
	if (n > 1) {
		printb(n >> 1);
	}
	printf("%d", n & 1);
}

void printLeo(int *p, int len, int *leo, int leolen, int leov, int leovp) {
	int i, j;		// Loop iterators
	int leosize;	// Size of this leo tree
	int buffer = 0;	// Count of already printed trees
	int row;		// For printing trees with more than one item

	// Print the binary representation of the bitstring
	//printf("Binary: ");
	//printb(leov);
	//printf("\n");
	// Loop through each bit of leov
	for (i=(sizeof(int)*8)-1; i>=0; i--) {
		// Check to see if there is a tree of this size
		if((leov >> i) & 1) {
			// Find the size of the tree
			leosize = leo[leolen-i-leovp-1];
			row = 0;
			//printf("Tree of size %i found at position #%i \n", leosize, buffer);
			// Loop through elements in that tree
			for (j=leosize-1+buffer; j>=buffer; j--) {
				// Print item
				printf("%i, ", p[j]);
				// Print a new row if we need to
				if (j && (int)log2(j+1) != row) {
					printf("\n");
					row = (int)log2(j+1);
				}
			}
			// Ok, finished printing the list. Let's not print it again
			buffer += leosize;
			printf("\n");
		}
	}
}

// This is the same as the siftDown function, but works for leftward-facing heaps
// Fun stuff, it means we have to use negative array indices ;)
uint64_t leoSiftDown (int *p, int *leo, int lenIndex) {
	uint64_t c = 0; // Comparison counter

	// Stop recursion once we get to individual leaves
	if (leo[lenIndex] < 3) {
		//printf("Tree size %i, ending siftDown\n", leo[lenIndex]);
		return c;
	}

	//printf("\nSifting down from %i, size %i(%i)...\n", p[0], lenIndex, leo[lenIndex]);
	// Get children
	//printf("Children: %i(%i), %i\n", leo[lenIndex+2], p[-leo[lenIndex+2]-1], p[-1]);

	// Try to sift rightwards first, otherwise go left
	c += 2;
	if (p[-1] > p[-leo[lenIndex+2]-1]) {
		// Right is greater, try switching with that
		//printf("Sifting right %i<->%i\n",p[0] , p[-1]);
		if (p[0] < p[-1]) {
			p[0] ^= p[-1] ^= p[0] ^= p[-1];
			c += leoSiftDown(&p[-1], leo, lenIndex+2);
			return c;
		}
	} else {
		// Left is greater, try switching with that
		//printf("Sifting left %i<->%i\n",p[0] , p[-leo[lenIndex+2]-1]);
		if (p[0] < p[-leo[lenIndex+2]-1]) {
			p[0] ^= p[-leo[lenIndex+2]-1] ^= p[0] ^= p[-leo[lenIndex+2]-1];
			c += leoSiftDown(&p[-leo[lenIndex+2]-1], leo, lenIndex+1);
			return c;
		}
	}
	return c;
}

// Move this new item leftward to the 
uint64_t siftLeft(int *p, int len, int *leo, int leolen, int leov, int leovp) {
	int i, j;		// Loop iterators
	int leosize;	// Size of this leo tree
	int buffer = 0;	// Count of already printed trees
	int prev = 0;	// Index of previous tree root
	uint64_t c = 0; // Comparison counter
	//printList(p, len);
	// Loop through tree sizes
	for (i=0; i<leolen; i++) {
		// Found a tree size?
		if((leov >> i) & 1) {
			leosize = leo[leolen-i-leovp-1];
			// Check to see if we found a tree yet
			if (prev) {
				// Compare this root to the previous one
				//printf("LeftSift: Comparing %i to %i\n", p[len-buffer-1], p[prev]);
				c++;
				if (p[prev] < p[len-buffer-1]) {
					// Swap them
					//printf("Swapping %i and %i\n", p[len-buffer-1], p[prev]);
					p[prev] ^= p[len-buffer-1] ^= p[prev] ^= p[len-buffer-1];
					//printf("Sifting down from %i\n", p[len-buffer-1]);
					c += leoSiftDown (&p[len-buffer-1], leo, leolen-i-leovp-1);
				} else {
					// Already largest, end.
					//printf("Sifting down from %i\n", p[prev]);
					c += leoSiftDown (&p[prev], leo, leolen-i-leovp-1);
					return c;
				}
			}
			prev = len-buffer-1;
			//printf("%i(%i), ", p[prev], leosize);
			buffer += leosize;
			//printf("Buffer: %i>%i?\n", buffer, len);
			if (buffer >= len) {
				// Reached the end of the thingy
				//printf("Sifting down from last tree: %i(%i), size %i\n", len-buffer-1+leosize, p[len-buffer-1+leosize], leo[leolen-i-leovp-1]);
				//printList(p,len);
				c += leoSiftDown (&p[len-buffer-1+leosize], leo, leolen-i-leovp-1);
				break;
			}
		}
	}
	return c;
}

uint64_t smoothSort(int *p, int len) {
	uint64_t c = 0;		// Comparison counter
	int i, j;			// Loop iterator
	int *leo;			// Array of leonardo numbers
	int leolen;			// Length leo array
	int leov;			// Bitvector for keeping track of leo trees
	int leovp;			// Counter of least significant bits

	// Get maximum number of Leonardo Heaps
	for (i=0, j=0; i<len; j=leonardo(i++)) {
		if (j >= len) {
			leolen = i;
			break;
		}
	}

	// Generate array of available leonardo numbers
	leo = malloc(leolen * sizeof(int));
	for (i=0; i<leolen; i++) {
		leo[leolen-1-i] = leonardo(i);
	}
	//printList(leo, leolen);
	leov = 0;
	leovp = 0;

	// Generate a series of Leonardo trees
	
	for (i=0; i<len; i++) {
		// Print current state
		//printf("\nI: %i(%i) - ",i, p[i]);

		// Insert the new item
		if ((leov & 3) == 3) {
			// Last two bits are set, merge the trees
			//printf("Merge trees (Order %i)\n", leovp+2);
			leov >>= 2;
			leov |= 1;
			leovp += 2;
			// Sift downwards
			//printList(p, i+1);
			c += leoSiftDown(&p[i],leo, leolen-leovp-1);
			//printList(p, i+1);
		} else if ((leov & 1 == 1) && (leovp == 1)) {
			// Last tree has order 1, add one of order 0
			//printf("Add a tree of order 0\n");
			leov <<= 1;
			leov |= 1;
			leovp = 0;
			c += siftLeft(p, i+1, leo, leolen, leov, leovp);
		} else {
			// Add a tree of order 1
			//printf("Add a tree of order 1\n");
			leov <<= (leovp-1);
			leov |= 1;
			leovp = 1;
			c += siftLeft(p, i+1, leo, leolen, leov, leovp);
		}
		//printb(leov);
		//printf(" : %i\n", leovp);
		//printList(p, len);
	}

	//printf("\n\nHeapify complete. Proceeding with disassembly...\n\n");
	//printLeo(p, len, leo, leolen, leov, leovp);


	// Dequeue the Leonardo heaps
	for (i=len-1; i>0; i--) {
		//printf("\nI: %i(%i) - ", i, p[i]);
		//printb(leov);
		//printf(" : %i (Size %i)\n", leovp, leo[leolen-leovp-1]);
		//printList(p, len);
		if (leovp > 1) {
			// Breaking down tree into smaller components
			//printf("Breaking down tree of order %i(%i) into smaller components\n", leovp, leo[leolen-leovp-1]);
			// Shift left one
			leov--;
			leov <<= 2;
			leov |= 3;
			leovp-=2;
			// Check if there is a left tree
			if(i-leo[leolen-leovp-3] >= 1) {
				//printb(leov);
				//printf(" : %i (Size %i)\n", leovp, leo[leolen-leovp-1]);
				//printf("Len: %i - %i - 1= %i\n", i, leo[leolen-leovp-1], i-leo[leolen-leovp-1]-1);
				//printf("Left-Sifting left tree from %i(%i)\n", i-leo[leolen-leovp-1]-1, p[i-leo[leolen-leovp-1]-1]);
				c += siftLeft(p, i-leo[leolen-leovp-1], leo, leolen, leov>>1, leovp+1);
				//printf("Left Tree\n");
				//printf("Checked\n");
			}
			if (i>1) {
				//printf("Left-Sifting right tree from %i(%i)\n", i-1, p[i-1]);
				c += siftLeft(p, i, leo, leolen, leov, leovp);
			}
			
		} else if ((leovp <= 1)) {
			// Last tree has order 1
			//printf("Last tree has order %i\n", leovp);
			// Flip the last bit
			leov--;
			// Shift it all rightwards
			while ((leov & 1) == 0) {
				leov >>= 1;
				leovp++;
			}
		} else {
			// Last tree has order 0
			//printf("Last tree has order 0\n");
		}
	}

	// Free stuff
	free(leo);

	printv("Smooth sort checks: %i\n", c);
	return c;
}

//
// Tournament Sort
//

// Create a tournament heap with (2^n)-1 nodes. n can vary depending on available memory
// Remove the top node and sift the rest upwards. Fill gaps in the leaves with new items.
// Removed items are placed in a priority queue, to be stopped when
// either all leaves are blocked or when we run out of new items
// Then repeat placing new items on a new priority queue
// Finally do a k-way merge with all the priority queues
// Time complexity is approx O(nlog(n))

// Pushes up, tournament style.
uint64_t pushUp (int *t, int *returnPointer, int *status, int len, int index, char ignore_siblings) {
	uint64_t c = 0;
	int leftIndex = children(parent(index));

	// If index is the root of the tree or if index is beyond the bounds of the tree
	if (!index || index >= len) {
		//printf("Returning #%i(%i, %i, %i)\n", index, !index, status[leftIndex], status[leftIndex+1]);
		return c;
	}
	if (status[parent(index)] == TOURN_EMPTY && status[leftIndex] == TOURN_FILLED && status[leftIndex+1] == TOURN_FILLED) {
		// Parent is empty, let's compare siblings
		c++;
		//printf("Comparing %i > %i\n", t[leftIndex], t[leftIndex+1]);
		if (t[leftIndex] > t[leftIndex+1]) {
			//printf("Moving R(%i) up to #%i\n",t[leftIndex+1], parent(index));
			t[parent(leftIndex)] = t[leftIndex+1];
			status[leftIndex+1] = TOURN_EMPTY;
			status[parent(index)] = TOURN_FILLED;
			//printf("Children of %i: %i\n",leftIndex+1, children(leftIndex+1));
			if (children(leftIndex+1) < len) {
				//printf("Pushing up from %i(%i)\n", children(leftIndex+1), t[children(leftIndex+1)]);
				c += pushUp(t, returnPointer, status, len, children(leftIndex+1), ignore_siblings);
			} else {
				*returnPointer = leftIndex+1;
				//printf("Setting returnPointer to %i\n", *returnPointer);
			}
		} else {
			//printf("Moving L(%i) up to #%i\n",t[leftIndex], parent(index));
			t[parent(index)] = t[leftIndex];
			status[leftIndex] = TOURN_EMPTY;
			status[parent(index)] = TOURN_FILLED;
			//printf("Children of %i: %i\n",leftIndex, children(leftIndex));
			if (children(leftIndex) < len) {
				c += pushUp(t, returnPointer, status, len, children(leftIndex), ignore_siblings);
			} else {
				*returnPointer = leftIndex;
				//printf("Setting returnPointer to %i\n", *returnPointer);
			}
		}
		// Do some recursion to move it further upwards
		c += pushUp(t, returnPointer, status, len, parent(index), ignore_siblings);
	} else if (status[index] == TOURN_FILLED && status[parent(index)] == TOURN_FILLED) {
		// Non-empty, let's compare parent and child
		c++;
		//printf("Comparing parent/child %i(%i) < %i(%i)\n", index, t[index], parent(index), t[parent(index)]);
		if (t[index] < t[parent(index)]) {
			// Yep, let's swap them
			t[index] ^= t[parent(index)] ^= t[index] ^= t[parent(index)];
			// Do it again recursively
			c += pushUp(t, returnPointer, status, len, parent(index), ignore_siblings);
		}
	} else if (status[parent(index)] == TOURN_EMPTY && ignore_siblings) {
		// Pull up whichever value is non- empty

		if (status[leftIndex] == TOURN_FILLED) {
			// Pull up left sibling
			//printf("Pulling up child %i(%i) from parent %i(%i)\n", leftIndex, t[leftIndex], parent(index), t[parent(index)]);
			t[leftIndex] ^= t[parent(index)] ^= t[leftIndex] ^= t[parent(index)];
			status[leftIndex] = TOURN_EMPTY;
			status[parent(index)] = TOURN_FILLED;
			*returnPointer = leftIndex;
			c += pushUp(t, returnPointer, status, len, children(leftIndex), ignore_siblings);
		} else if (status[leftIndex+1] == TOURN_FILLED) {
			// Pull up right sibling
			//printf("Pulling up child %i(%i) from parent %i(%i)\n", leftIndex+1, t[leftIndex+1], parent(index), t[parent(index)]);
			t[leftIndex+1] ^= t[parent(index)] ^= t[leftIndex+1] ^= t[parent(index)];
			status[leftIndex+1] = TOURN_EMPTY;
			status[parent(index)] = TOURN_FILLED;
			*returnPointer = leftIndex+1;
			c += pushUp(t, returnPointer, status, len, children(leftIndex+1), ignore_siblings);
		}

	} else {
		//printf("Nothing to do.\n");
	}
	return c;
}

uint64_t tournamentSort(int *p, int len) {
	uint64_t c = 0;			// Comparison counter
	int i, j, r;			// Iterators
	int* tourn;				// Array which contains the tournament heap
	int* tstatus;			// Array which holds tournament status information
	int tournSize, leaves;	// Size of tournament array
	piles stacks;			// Piles struct to perform a k-way wort
	int returnPointer = 0;	// Index at which to return after a pushUp
	int totalLen = 0;		// Length of 'sorter' arrays
	int sublen = len;		// Length of sub-array

	// Initialise the tournament heap
	tournSize = TOURN_SIZE;//pow(2, (int)log2(len)-1) - 1;
	leaves = tournSize / 2 + 1;
	tourn = calloc(tournSize, sizeof(int));
	tstatus = calloc(tournSize, sizeof(int));

	// Initialise the piles struct
	pilesInit(&stacks);

	// Loop through rounds of tournaments
	r = 0;
	while (sublen > 0) {
		// Start round

		// Build the tournament tree
		for (i=0, j=0; i<leaves && j<sublen; i++, j++) {
			// Start to insert a new item into the tournament array
			returnPointer = 0;

			// Insert into tournament array if empty
			if (tstatus[leaves-1+i] == TOURN_EMPTY) {
				// Add a new item into a leaf
				tourn[leaves-1+i] = p[j];
				tstatus[leaves-1+i] = TOURN_FILLED;
			} else {
				// Move to next leaf
				j--;
				continue;
			}

			// Push/sift it upwards
			c += pushUp(tourn, &returnPointer, tstatus, tournSize, leaves-1+i, 0);

			if (returnPointer) {
				// Shifting back to (returnPointer - leaves + 1);
				i = returnPointer - leaves;
			}
		}

		// Pull up any empty values to the top
		for (i=tournSize-1; i>0; i--) {
			if (!tstatus[parent(i)]) {
				c += pushUp(tourn, &returnPointer, tstatus, tournSize, i, 1);
			}
		}

		// Now pop items off the top of the tournament and place them into the piles struct
		// Set things up for this round
		i = tournSize; // Change $i to be at the end of the pulled items so far

		// Add a new pile
		newPile(&stacks);

		// Loop until we reach the end of the list
		while (i < sublen || stacks.pilesLen[stacks.len-1] < sublen) {
			// Start with pulling out stuff
			// Push into 2nd pile
			if (tstatus[0] == TOURN_FILLED) {
				tstatus[0] = TOURN_EMPTY;
				addToPile(&stacks, stacks.len-1, tourn[0]);
				//stacks.piles[stacks.len-1][stacks.pilesLen[stacks.len-1]++] = tourn[0];
				totalLen++;
			} else {
				break;
			}

			// Sift upwards, leaving a gap at the bottom of the tournament tree

			//
			// !! This bit needs to pull up even when there are no sibling items
			//
			returnPointer = 0;
			c += pushUp(tourn, &returnPointer, tstatus, tournSize, 1, 1);

			// Insert a new item into the gap
			if (tstatus[returnPointer] == TOURN_EMPTY && i < sublen && returnPointer >= (leaves-1)) {
				tourn[returnPointer] = p[i++];

				// If it's less than the higest value in the list, lock it
				if (tourn[returnPointer] < stacks.piles[stacks.len-1][stacks.pilesLen[stacks.len-1]-1]) {
					tstatus[returnPointer] = TOURN_LOCKED;
				} else {
					// Good to go, push it upwards
					tstatus[returnPointer] = TOURN_FILLED;
					c += pushUp(tourn, &returnPointer, tstatus, tournSize, returnPointer, 0);
				}
			}
		}

		// Rearrange p* to have the missing items at the front
		// Pull from the end of p*
		for (j=i, i=0; j<sublen; j++) {
			p[i++] = p[j];
		}
		// Check leaves for any locked values
		for (j=0; j<leaves; j++) {
			if (tstatus[leaves-1+j] == TOURN_LOCKED) {
				p[i++] = tourn[leaves-1+j];
			}
		}
		sublen = i;
		// Clear tournament arrays
		for (j=0; j<tournSize; j++) {
			tourn[j] = 0;
			tstatus[j] = 0;
		}
	}

	// Merge the piles together
	c += mergePilesToArray(&stacks, p);

	// Finish things off
	free(tourn);
	clearPile(&stacks);
	printv("Tournament sort checks: %i\n", c);
	return c;
}

//
// Weak Heap Sort
//

// A weak heap is a heap-like structure with some creative liberties that make it less strict
// It uses a binary array to keep track of whether a node's children are in reverse order
// Items are sorted by first constructing a weak heap and then picking the top item and re-balancing
// Time complexity O(nlog(n)-n/2)

// Gets the distinguished ancestor
int whParent(int i, binarr *r) {
	// Check if it is the right child or not
	// (Odd ones are right children by default)
	while ((i & 1) == binarrGet(r, i/2)) {
		i /= 2;
	}
	return (i/2);
}

// Get the left child of an index
int whLChild (int i, binarr *r) {
	return (i*2 + binarrGet(r, i));
}

int whLeftmostChild (binarr *r, int len, int child) {
	int temp;
	if (whLChild(child, r) < len) {
		return (whLeftmostChild (r, len, whLChild(child, r)));
	} else {
		return (child);
	}
}

// Get the right child of an index (not used)
int whRChild (int i, binarr *r) {
	return (i*2 + 1 - binarrGet(r, i));
}

void whPrint (int *p, binarr *r, int len) {
	int row = 2;
	printf("%i\n%i",p[0],p[1]);
	//Loop through pairs
	for(int i=2; i<len; i+=2) {
		if (i && (int)log2(i) != row) {
			row = (int)log2(i);
			printf("\n");
		}
		// Print pair in correct order
		if (binarrGet(r, i/2)) {
			printf("%i, %i, ", p[i+1], p[i]);
		} else {
			printf("%i, %i, ", p[i], p[i+1]);
		}
		
	}
	printf("\n");
}

char whCheck (int *p, binarr *r, int len) {
	// Loop through each item and compare it to it's distinguished ancestor
	for (int i=len-1; i>0; i--) {
		// Compare with distinguished ancestor
		if (p[whParent(i, r)] < p[i]) {
			printf("Weak heap Bad at %i(%i) < %i(%i)\n", whParent(i, r), p[whParent(i, r)], i, p[i]);
			return 0;
		}
	}
	printf("Weak heap Good\n");
	return 1;
}

// Function to join two trees together
uint64_t whJoin (int *p, binarr *r, int i, int j) {
	if (p[i] < p[j])
    {
        binarrFlip(r, j);
        p[i] ^= p[j] ^= p[i] ^= p[j];
    }
    return (1);
}

// Function for sifting up to build the heap
uint64_t whSiftUp (int *p, binarr *r, int i) {
	uint64_t c = 0;
	while (i > 0) {
		c += whJoin(p, r, 0, i);
		i /= 2;
	}
	return c;
}

uint64_t weakHeapSort(int *p, int len) {
	uint64_t c = 0;			// Comparison counter
	binarr binr;			// Binary array representing where there are reverse children
	int i, j, x, y;			// Loop iterators

	// Initialise the binary array
	binarrInit(&binr, len);
	binarr* r = &binr;
	//printf("Len: %i\n", r->len);

	// Build a weak heap
	for (i = len - 1; i > 0; --i) {
		c += whJoin(p, r, whParent(i, r), i);
	}

	// Print some stuff
	//whCheck(p, r, len);
	//whPrint(p, r, len);
	//printList(p, len);
	//binarrPrint(r);
	

	// Loop through items
	for (i=len-1; i>1; i--) {
		// Move top item to the end
		p[i] ^= p[0] ^= p[i] ^= p[0];
		
		// Sift it upwards from the lowest leftmost child
		c += whSiftUp (p, r, whLeftmostChild (r, i, 1));
	}
	// Swap the last two items
	p[1] ^= p[0] ^= p[1] ^= p[0];

	binarrFree (r);
	printv("Weak heap sort checks: %i\n", c);
	return c;
}