# Sorting in C

This is a little experiment to create the functionality and test the effectiveness of sorting algorithms. Some of these don't have publicly available code online so I tried to reproduce them as closely as possible just from their descriptions.

## Usage

The default program executes a single sorting algorithm 100 times. The algorithm is in the code, so this is mostly used for development.

### Options

	--verbose

This prints out detailed information about how each algorithm is being processed. Good for debugging, but will slow down exports by a lot.

	--csv <option>

This runs every algorithm a number of times and then exports it to a csv.

Option `b` for 'basic' exports the raw results of each individual run, without calculating the average. This is good for seeing how often a run of a particular size appears, as in a best and worst scenario and how the spread is shaped.

Option `d` gives a detailed view, which runs the algorithm on lists of length 10-n with gaps of 10. Good for seeing how each algorithm's time increases as the size of the input list increases.

	--algo <funcName>

This runs a specific algorithm. To use it, write the camelCase name of the algorithm eg `sort --algo bubbleSort`. It will then perform a sort on a list of size 10.

	--test <funcTypeIndex>

This runs a test on all algorithms of this category. The cetegories and indices are as follows:

0. Exchange Sorts
1. Insertion Sorts
2. Selection Sorts
3. Random Sorts
4. Merge Sorts
5. Distribution Sorts
6. Concurrent Sorts

And these are the kinds of lists being input into them (so far):

* Complete unique integers 0-N
* Random integers 0-N (may have gaps or repeats)
* Repeated integers 0-sqrt(N)

## Exchange sorts

These are algorithms that sort by exchanging items in-place, and usually don't require any additional memory.

### Bubble Sort

The simplest sorting algorithm of them all. Bubble Sort starts at the left and moves rightwards, switching the higher item towards the right till it reaches the end. It repeats this each time, skipping the last items in the array since they are in their correct position. The sorted list grows till it reaches the left.

Time complexity is O((n^2-n)/2 - [Perm]), where Perm is the chance of a pre-sorted list arising towards the end of the sequence (when the 'unsorted' portion of the array gets smaller and smaller).

### Comb sort

Starts with a gap of length/2 and compares items with those a gap's length away, decreasing the size of the gap each time. Good for getting a kinda-sorted list fairly quickly, with items being placed close to their correct position fairly early.

Time complexity O((n^2)/2 - ((n/2)^2)/2 - [Perm])

### Cocktail sort

Just like bubble sort, but adds items by laternating the direction of comparisons. Useful if moving the pointer to the beginning of the list is not possible in O(1) time. Kind of like a Gnome sort where the gnome can carry leftwards as well.

Same time complexity as Bubble Sort: O((n^2-n)/2 - [Perm])

### Proportional Exchange Sort

A strange one. It starts with a small pre-sorted list S and then splits it along its median M to make (SL)M(SR). It then takes an unsorted chunk U and sorts it using Quick Sort around the median creating (SL)(UL)M(UR)(SR). I might have gotten the algoritm wrong since sorting SL with UL and UR with SR causes a lot of inefficient re-ordering, but in theory it should be about as good as Quick Sort but with a much better worse-case scenario.

Time complexity O((n^2)/2).

### Quick sort

The classic sorting algorithm against which all other fast algorithms are compared. It picks a pivot at random nad then puts all lower items to the left and all greater items to the right. It then continues recursively by sorting each side. It saves a lot of time since the left items will no longer have to be sorted with the right items after they have already been compared with the pivot.

Time complexity O(nlogn) but has a worst-case scenario if each pivot is chosen from the extremes rather than the middle.

### Slow Sort

Aka **Silly Sort** or **Spaghetti Sort**. It splits the array into two, sorts both halves recursively then picks out the max item and places it at the end.

Time complexity O(n^(log2(n)/2)

### Stooge Sort

Splits the array into thirds, then sorts the first two thirds, the the last two, then the first again. Does this recursively.

Time complexity O(n^(log2(3)/log2(1.5)))

## Random sorts

These are a subset of Exchange Sorts that reply on either some kind of randomisation or just good old fashioned brute force. Because of that, their time complexity can be very unpredictable.

### Bogo Sort

Aka **Monekey Sort**, **Stupid Sort** **Permutation Sort**, this algorithm is (on average) the slowest out there. It randomises the array and checks to see if they're in order. If not, it shuffles them randomly and tries again.

It has a good best-case scenario of O(n) if the array is pre-ordered but on average works on O(x!(e-1)). Worst-case scenario is awful, it could be O(∞) if the shuffling is very unlucky.

### Bozo Sort

Works a lot like Bogo Sort, but randomly swaps two items at a time before checking if the array is sorted. There is also a smart version of this algorithm that only swaps if it would make the list more sorted.

**Dumb Bogo Sort** (unsurprisingly) works about as fast as Bogo Sort since you're basically randomly picking a new permutation each time. The only difference with Bogo Sort is that the new permutation is almost identical to the old one. It performs O(n!) swaps and O(n!\*e) comparisons when checking the array's sortedness.

**Smart Bozo Sort** generally works a lot better except for with small numbers. It performs O(n^5.5) swaps which grows faster than the dumb version's O(n!) for values below n=8. As for total comparisons, it stays under the dumb version until about n=10. The reason for this is that the smart version will spend more time with an almost-sorted list so it'll have more comparisons per swap in the later stages of the sort. Total comparisons are about O(8\*n^5.5). Experimental tests have shown a very volatile time complexity due to the random nature of the algorithm.

In terms of comparisons per swap, Dumb Bogo Sort does about *e*. Smart Bogo Sort does about *n/2*. This would be harmful for higher *n*s, but by the time it becomes significant, the factorial really starts to turn up the heat.

### Brute Force Sort

I would much rather call this algorithm *Permutation Sort*, but people typically use that term as an alternate name for Bogo Sort. Therefore I prefer to call it *Brute Force Search* because it is based on the encryption-cracking technique known as brute-forcing. It exhaustively lists every possible permutation and checks to see if it is sorted. It really is that simple.

It produces **n!** possible permutations, so the worst-case scenario would be that the permutation generated is the one that is sorted. Since every permutation is equally likely to be sorted, it averages out to taking **O(n!/2)** permutations before finding the right one.

In terms of actual comparisons, it does about **O(0.9n!)**. That coefficient doesn't sit right with me, making it 1.8 comparisons per permutation. It should have something to do with `e` but I'm not sure how to quantify it.

### Goro Sort

The page for the Google Code Jam that had the description for Goro Sort is offline, so here is the original text:

> Goro has 4 arms. Goro is very strong. You don't mess with Goro. Goro needs to sort an array of N different integers. Algorithms are not Goro's strength; strength is Goro's strength. Goro's plan is to use the fingers on two of his hands to hold down several elements of the array and hit the table with his third and fourth fists as hard as possible. This will make the unsecured elements of the array fly up into the air, get shuffled randomly, and fall back down into the empty array locations.
>
> Goro wants to sort the array as quickly as possible. How many hits will it take Goro to sort the given array, on average, if he acts intelligently when choosing which elements of the array to hold down before each hit of the table? Goro has an infinite number of fingers on the two hands he uses to hold down the array.
>
> More precisely, before each hit, Goro may choose any subset of the elements of the array to freeze in place. He may choose differently depending on the outcomes of previous hits. Each hit permutes the unfrozen elements uniformly at random. Each permutation is equally likely.

So it's basically a smart Bogo Sort where you 'lock' down the elements that are in their correct position after every iteration. 

The original question from the Google Code Jam was to find out how many slams that Goro would have to make before the array would be sorted. Turns out that for any list `n`, the slam complexity is `O(n)`, plus minus a few degrees of deviation. This means that since each element has a `1/s` chance of being in the right position (where `s` is the number of unloacked elements), we're expecting to have one additional element secured after each slam. However since this is a probabilistic sort, it's not guaranteed.

In terms of its actual time complexity, it's `O(2*n^2)`, making it pretty good for a sort that relies on shuffles to move elements around. However, it does rely on having a sorted list with which to compare itself to. This version uses Quick Sort so I suppose that the true time complexity would be `O(2*n^2 + nlog(n))`.

## Insertion sorts

Rather than swapping elements in the array with each other, these sorting algorithms tend to put their elements into a sorting data structure and then back in the array.

### Gnome Sort

Moves one step at a time. Moves leftward picking the smallest element (like Insertion Sort), and then moves rightward until it finds an item out of order.

Time complexity is like Bubble Sort: O((n^2 -x)/2 - [Perm])

### Insertion Sort

A classic sorting algoritm. This one starts with a sorted list (initially only one item) and moves the next item leftward until it reaches its correct position.

Time complexity is O((n^2-n)/4) 

### Library Sort

The algorithm copies the array into another array that is twice as long with gaps in between each element. Items are then taken from the original list and inserted into the gaps using a binary sort. This version reshuffles when a conflict is found, but a more optimised version with insert it using Insertion Sort.

Time complexity of this version is O((n^2+n)/2.

### Patience Sort

Also known as Solitaire Sort, this algorithm places each item into a pile if it is greater than the last item added, and creates a new pile if all piles have greater 'last' items. It then does a k-way merge to put all the ordered piles together which would create our sorted array.

Time complexity: O(nlog(n)\*1.5)

### Shell Sort

This generates a sub-array of elements that compares items that are far away first to make sure that they jump closer to their target quicker.

Time complexity: O((n^2)/6)

### Splay Sort

Creates a splay tree by inserting items on the root of the tree by splaying it, placing the most recent items closer to the top.

Time complexity: O(nlog(n)+n), but requires an extra structure that is 3n in length like Tree Sort.

### Tree sort

Creates a Binary Search Tree by placing new items at the bottom of the tree on their appropriate branch.

Time complexity is O(nlogn+n), but like Splay Sort, also requires a pretty large structre to run.

## Selection Sorts

These are algorithms that server to find the kth smallest or largest element and then adding it to the beginning or end of the array.

### Bingo Sort

Bingo sort is a variation of the more popular Selection Sort but with a slight modification. Instead of just looking for the lowest item one at a time, it keeps track of each repeated 'copy' of that lowest item. This allows it to select all copies and place them on the left at the same time.

This makes it more efficient than Selection Sort when there are repeated items, but requires additional memory to hold the queue.

Time complexity of Bingo Sort is O((n^2-n)/2 - k), where `k` is the reduction gain from processing multiple items in one iteration. It's hard to really quantify how much this gain is because it depends on a few factors:

* How many repeated items there are (more items skip more loops)
* How close they are to the beginning (lower items have a bigger reduction)

So basically, each repeated item reduces the O((n^2-n)/2) time by how many items there are greater or equal to it. For example, in a randomised list of 10 items like `[0,1,1,1,4,5,6,7,8,9]`, the first '0' does nine comparisons and the first '1' does eight just like in Selection Sort. But since the 2nd and 3rd '1' are skipped, it doesn't perform seven or six comparisons. The following '4' does five comparisons, the '5' does four etc as normal.

The reduction really does depend on the input list so it's hard to quantify how large `k` really is. For most tests, I've been using an array of unique elements, so for comparison's sake it's about as fast as Selection Sort but with some redundant memory.

### Cartesian sort

Cartesian sort is kind of like a mixture between Tree Sort and Selection Sort. First it puts the array into a Cartesian tree, which is a tree that follows the heap property (child nodes are always lower/higher than their parent). This means that the root is always the smallest/largest element.

Then it disassembles the tree starting from the root node where it puts each element in a priority queue and selects the smallest one. The smallest node's children are then added to the queue and the process is repeated until the queue is empty.

The time complexity for this algorithm is O(nlog(n)/2+(n^2)/6), where the first O(nlog(n)/2) refers to the building of the Cartesian Tree, and O((n^2)/6) is the selection of the lowest element.

The best case occurs when the input array is in reverse order, which makes the Cartesian tree a linked list of nodes using the 'left' branch pointer. This uses O(n-1) comparisons since the priority queue section is skipped because it always has a max length of 1. The tree-building section is also short since each new element is only ever compared to the previous one and then made into its parent.

The worst case is when the list is pre-sorted. This makes a linked list just like the best-case, but this time it's right-heavy. This means that every new item needs to be compared to every other previous item before being added in. The Selection Sort section is still only one item wide, so it can be skipped with zero comparisons made. There might be a worse worst case if somehow the Selection Sort section becomes very large, but I couldn't find a consistent way of getting such an input.

### Cycle Sort

Cycle Sort starts off with the first item, then counts up the items that are lower than it and places it in the correct position. It then takes the element what was in that position and does the same until it returns to where the first item was taken from. It thengoes to the next item and starts a new cycle from there.

It's a fairly simple algorithm whose advantage occurs where comparisons are cheap but writing to the array is expensive.

Time complexity O(1.5\*n^2). Pretty bad, but only performs O(n)writes to the array.

### Heap Sort

This algo converts the array into a heap array where each element n has a parent that is (n-1)/2. Each child must be smaller than its parent - this is achieved by using the siftDown function where a parent is compared to its two children and swapped with the higher of the two if needed.

The heap is then sorted by going through the heap and swaping the top element (the highest one) with the one at the end (may or may not be the lowest) and sifting down. It then reduces the length of the unsorted array by one.

Time complexity O(nlog(n)\*1.5). It's pretty good since it uses a binary tree structure but without having to create a whole separate struct so it takes practically zero extra memory since it's all processed in-place.

### Selection Sort

Looks rightward for the lowest item and swaps it with the leftmost unsorted item. A bit worse on average than Bubble Sort since it doesn't have permutation solve, but useful if swaps are expensive to execute.

Time complexity O((n^2-n)/2). Pretty much like Bubble Sort.

### Smooth Sort

Kind of like Heap Sort but instead of having one big tree with its root on the left, it has many trees with right-rooted heaps. These heaps' sizes are based on Leonardo numbers, so they can have sizes of [1, 1, 3, 5, 9, 15, 25, ...].

Like Heap Sort, the algorithm works in two stages: heapify and dequeue.

Heapify traverses the list rightwards, with each new item *n* in the list can be either a new tree of size 1 or a root joining up the previous two trees. If it's a tree merge, the new value is sifted down to swap with its children. If it's a new tree, it's sifted leftwards to keep the highest root on the rightmost tree, and each swap causes a sift down opetation to maintain the heap structure.

Dequeueing does the same thing in reverse. If it finds a tree root, it breaks the tree and sifts left from the rightmost child, then does the same with the left child. If it's a tree with a single node, it just dequeues it.

[This is a great article on the subject of smoothsort](https://www.keithschwarz.com/smoothsort/).

It's more inefficient than quicksort or heapsort at O(nlog(n)\*3) on average. However, it has a pretty good with a sorted or almost-sorted list at O(n\*4).

### Tournament sort

Works like Heap Sort, except that the heap doesn't cover the whole input array. Instead it only covers a small tournament tree which has a size of (2^n)-1. It then feeds in items from the input to the bottom of the tournament tree and pulls them upwards.

The next step is to pull the top item one by one into a priority queue and inserting subsequent items into the bottom of the tree and pushing them upwards. When a new item is added to a leaf and it is lower than the highest item in the queue, it is 'locked'. If all leaves are locked, the round ends and no new items are added. A new round begins and all locked and unprocessed items are pulled through the same process.

Finally, the priority queues are put through a k-way merge to get the final sorted list.

Time complexity O(nlog(n)) for small tournament trees, larger trees can increase the time even with big lists. However, this algorithm does *many* checks with the current status of the tree, to check if it's an empty, filled or locked slot. If these comparisons are taken into account, the time complexity would increase substantially.

Tournament Sort also uses quite a lot of auxilliary memory: A tournament array, a tournamnet status array and priority queues as well. This could be optimised by cleverly using pointers in the source array.

### Weak Heap sort

A weak heap is a heap-like structure (see Heap Sort) with some loose rules:

* Root has no left sub tree
* Right children must be lower (or higher if it's a min heap)
* Leaf nodes are only allowed on the bottom of the tree

The second point means that the specificity of left/right children is important to maintaining a weak heap structure since left children can be out of order but not right children. To do this, a binary array is used to keep track of which child is the left one and which is the right one. The left child is located at 2n and the right at 2n+1, but if the binary array bit is set, then it's the other way around.

The first step to performing this sort is heapifying the array by building a weak heap. This is done by comparing each item with its distinguished ancestor. For right children, this is their parent; for left children, it goes up its parental lineage until it reaches a right child. It then swaps the two if needed. The swapping also flips the ancestor's bit in the binary array.

The second step is to perform the actual sort. For each step, the top item is removed from the weak heap and then the heap is rebalanced by sifting upwards from the lowest leftmost child.

The algorithm completes in O(nlog(n)-n/2) time, which makes it slightly faster than most nlog(n) sorts. However, it does require auxiliary memory for the binary array of size n/8, and it performs a lot of checks with this binary array to find specific items in the list.

## Merge sorts

This is Merge Sort and its variations.

### In-place Merge Sort

The In-place Merge Sort is a sorting algorithm that performs a merge-like fusion but instead of using individual elements as the starting point, it first checks the array for any pre-ordered sub-lists. This helps to cut down the time spent performing merges at the expense of spending a bit more time at the beginning searching for these sub-lists.

The in-place nature of this means that is uses a lot less auxiliary memory and fewerrecursive steps, but it has to do a lot more swaps to shift elements around the array.

Time complexity is O(0.9nlog(n)) with a somewhat large standard deviation due to its reliance on having pre-sorted arrays. It will be more effective on almost-sorted arrays and less effective on reversed arrays, where it is basically just Merge Sort with a redundant sub-list check at the beginning.

### Merge Sort

This algorithm splits the array into two ordered sublists which are then merged using a 2-way merge. The two sublists are in turn sorted recursively by splitting them in half as well.

Time complexity is O(nlog(n)-n), which is slightly faster than even Weak Heap Sort, and has alot less overhead since it has no need to manage a binary array.

### Polyphase merge sort, Oscillating merge sort and Cascade merge sort

These sorting algoritms are basically just merge sort except that instead of sorting an array, they're used for sorting while merging different files together. Good for large external sorts, but not really useful for the purposes of this program.

## Distribution Sorts

These are sorts that don't compare element's values but rather sorts by looking at their actual values. They're typically faster than other sorting algorithms since they are working with more information.

### American Flag Sort

This is based on the Dutch Flag Sort, which sorts elements into a beginning, middle and end group but instead uses any number `n` of groups greater or equal to 2. In practice it works a lot like MSD Radix Sort, but keeps everything in-place rather than relying on auxiliary memory. It does need a series of radix pointers to keep track of where every radix group begins/ends.

American Flag Sort goes from left to right in the array swapping the current element with the leftmost unsorted element in the corresponding radix group. It then does it again with the newly swapped element and moves rightward if it's already in its correct place. This means that the `i` loop iterator typically spends more time in the lower end of the array than in the end since it repeats quite a lot a the beginning and then does a lot of skipping.

Like other distribution sorts, it's linear. In fact, it's almost identical to both Radix Sorts except a little bit slower. O(n+2.35\*nlog10(n)) but it does save quite a bit of memory since it only needs O(2\*base) memory for each level of recursion compared to O(n) for MSD Radix Sort.

### Bead Sort

Bead Sort (aka. Gravity Sort) is an algorithm that attempts to simulate the natural ordering that gravity provides by splitting a number into unary digits (tallys in a binary array) and then have it fall sideways to the bottom.

Imagine an abacus or a Connect-Four board where each element is stacked up with the amount of tokens corresponding to its number. Ie, an element of '4' would have four beads stacked vertically. Now rotate the board (and the rails inside the board) 90 degrees so that the tokens fall according to gravity into the right place.

The tallys are then reassembled into whole numbers for the final sorted list.

This sorting algorithm has two methods to it: Fast and slow. The slow method drops each token one space at a time, while the fast one drops it all the way down in one go. The way that other people write the script tends to be the fast method, but this one has the problem that it creates incomplete rows after every iteration and makes it very hard to visualise.

Time complexity for the fast version is O(2.5n^2+n). Slow version is O(0.5(n^3+n^2)+n). They're both done in polynomial time, but the slow method is cubic instead of quadratic.

Space complexity is also pretty bad. I've made it more efficient than other implementations by using one bit per token 'slot', but it is still O(n\*`k`) where `k` is the size of the largest value. The bitwise evaluations reduce the space complexity to O(n\*`k`/8)

### Bucket sort

Bucket Sort is eerily similar to MSD Radix Sort, but with one difference. Instead of placing items into buckets based on a power of a particular base, it's done by splitting based on fixed-size buckets. What this means is that MSD Radix Sort uses a fixed bucket *count* while Bucket Sort uses a fixed bucket *size*.

The fixed bucket size has the problem that we can no longer use Bucket Sort to sort the items recursively; we need to rely on another sorting algorithm for the second stage of sorting. In this case, we're using Insertion Sort.

The items are then re-inserted into the array by traversing the pile in order.

Time complexity O(5.2n)

### Burst Sort

Burst Sort (aka Trie Sort) is a sorting algorithm primarily used to sort strings, but this particular instance allows the sorting of integers as well. This is done by using a similar method to MSD Radix Sort and treating each digit as a 'character' in a string. The algorithm then puts it all in a trie data structure and then unfurls it back into the starting array.

Approximate average time complexity is O(n\*(ceil(log(n))\*2-1)) since it depends on how spread out the elements are. Space complexity is about O(10\*nlog(n)).

### Counting Sort

The most basic distribution sort. It starts by finding the largest number and creates an array of zeroes of that size (note that this means that this version only works for non-negative ints. It then goes through the array and tallies the quantity of each element in its corresponding position. Finally, the secondary array is unfurled into the original; going through each value ind inseting as many elements as have been tallied.

It has a very low linear time complexity of O(2n+k) (one *n* for finding the max, another *n* for tallying and *k* being the max value). However, it has a fairly large space complexity since the size of the generated array is dependent on the max value *k* rather than the size of the initial array *n*. This is solved by other distributions sorts like Spaghetti Sort or Radix Sort.

### Flash Sort

Flash Sort takes a number of equally-sized buckets `b` between the maximum and minimum values in the array and counts the number of elements within them. It then converts these counts into array indices and runs through each item in its bucket.

It does this by using the formula in `getFlashBucket()` on each element, and then using the returned bucket number to find its correct position in the original array by using the indices array. This is repeated until it gets an element that is already in its correct position, in which case it just moves forwards until each item is in its correct bucket.

This produces an almost-sorted array in O(n) time, with each bucket having similar numbers grouped together. It can then be sorted by some other algorithm; in the case I used it is Insertion Sort. Insertion Sort is pretty slow at O(n^2 / 4), but it can be quite fast with an almost-sorted array. 

The time complexity for this sort is a quadratic equation that depends heavily on the number of buckets `b`. With `b=10` buckets, the coefficient of the `n^2` portion becomes 0.03; and with `b=128`, it becomes 0.001. It seems like a no-brainer to use many buckets but this would not only increase the size of the auxiliary array but also increase the other constants in the quadratic formula besides n^2 (ie bn + c) which makes short lists solve a lot slower.

I think that it's possible to pick a best bucket count for each input, but I haven't figured it out yet so I'll leave it till a later date.

### Interpolation Sort

This sorting algorithm is very similar to counting sort, but instead uses the interpolation formula to calculate each element's position. This means that instead of having an array of all possible integers between zero and the maximum, it only deals with an array of size O(n).

So while it does take less memory, it does require more passes. The time complexity of Interpolation Sort is O(5n), which if you have a set of [0, 1, 2, 3, etc], is much more inefficient than counting sort. It's a similar case when you have many small, repeated items. Where it shines though is where Counting Sort's *k* is very large but *n* is small. This will have very short loops and use very little memory.

### Pigeonhole Sort

Pigeonhole Sort is almost identical to Counting Sort, but with two differences:

* It only requires `max - min`-length auxiliary array instead of a `max`-length array
* The auxiliary array holds values themselves rather than just counts, making it a stable sort

The time complexity of Pigonhole Sort is O(r+4n), where `r` is range between the max and min items. If you only include elements in the auxiliary array that have objects in them, it gets reduced to O(4n).

### Proxmap Sort

Proxmap Sort is kind of a variation of Flash Sort in that the objective is to split the array into buckets of almost-sorted subsets and then use Insertion Sort to complete the sort.

The normal version of Proxmap Sort uses the floor function to determine the bucket of an item. So numbers like 6.3 and 6.48 would both go in the `4` bucket. This makes it pretty good for sorting arrays of floats since floats with the same whole parts will be in the same bucket and thus in close proximity.

Since this is a sorting algorithm that sorts integers, my approach is a little bit different. Rather than using a floor function, I use a "Divide by `PROXMAP_FACTOR`" function. This takes the item's value and divides it by `PROXMAP_FACTOR` which will determine which bucket it will go into. For example, with a factor of `2`, values of 9 and 8 would go into bucket #4 and values of 1 and 0 would go into bucket #0. This creates the same sort of effect as the floor function where instead of rounding down to the nearest integer, the integers get factored down to their appropriate quotient.

By using this method, we can then count the number of items in each bucket and put that into `hitCount[]`, and then we can use that to generate a prefix sum `proxMap[]`. By re-applying the `PROXMAP_FACTOR` function, we can calculate the positions of each element by using `hitCount[]` to offset `proxMap[]` and place those into `location[]`. They are then simply placed into the auxiliary array `p2[]` using the indices from `location[]` and finally back into the original array.

At this point the list is almost sorted. It just needs to use an algorithm like Insertion Sort to finish things off.

The time and memory complexity vary by what value is used as the `PROXMAP_FACTOR`. Generally speaking, a smaller value will make the script faster but take up more memory since you have a higher bucket count but have to do fewer comparisons at the Insertion Sort stage.

The time complexity usually ends up being somehwere between O(7n) and O(10n). They both grow linearly so it doesn't really make much of a difference for larger sizes.

### Radix sort

Radix sort is like counting sort, but using individual digits rather than the whole number.

**Least Significant Digit (LSD)**

The more classical method of the two starts off with the 'ones' digit by putting each number ending in a particular digit into their own bucket. For example, 01 and 31 would be in the '1' bucket. The algorithm then undoes the buckets by placing them back in the array. It repeats this for the 'tens' digit and then the 'hundreds' digit until all digits are covered.

**Most Significant Digit (MSD)**

This alternate method of Radix Sort starts from the left and works itself towards the right. Simply doing LSD Sort in reverse doesn't sort the array so a different approach is used. After putting each item into its bucket, it then recursively sorts that bucket according to the next digit in the list. Once it gets to a single-item it, it returns to its parent.

**Time and space complexity**

Radix Sort (both versions) have a time complexity of O(n+2\*nlogb(n)), where `b` is the numerical base in which the digits are represented. This means that a lower base (eg 2) will be slower. However, since there are fewer buckets at a time it takes up less memory to process.

MSD is slightly faster since it does do some skipping with longer strings / higher numbers, but due to its recursive nature it uses up more memory since it has to hold on to multiple instances of the buckets simultaneously.

## Concurrent Sorts

These are sorts that sort in parallel, allowing for the use of multiple threads to speed up the process.

### Batcher's odd-even mergesort

I must admit that this is one that I don't entirely comprehend this algorithm, but this is one way of looking at it:

* First, the input array is divided into two halves, and each half is recursively sorted using the same algorithm.
* Once the two halves are sorted, they are merged back together using a modified merge algorithm that performs a sequence of odd-even transpositions.
* Specifically, the merge algorithm compares adjacent pairs of elements in the two subarrays and swaps them if they are out of order, then performs a series of odd-even transpositions to ensure that all elements are in the correct order.
* The merge algorithm is then recursively called on the two halves of the merged array until the entire array is sorted.

This algorithm is like an unholy mixture of Merge Sort, Odd-Even sort and Bitonic Sort. Not something I'd really want to use.

Time complexity is O(n^2), but there does seem to be some spikes where n is a power of 2 that then taper off back to the n^2 curve. Kind of pretty actually.

### Bitonic sort

This sorting algorithm that relies on what's called 'bitonic sequence'. These sequencess have the property that they have 'triangular' appearance, in the sense that they always increase and then always decrease in an alternating fashion. The bitonic sequences start small with only a length of 2, but then double in size until they reach the full length of the array - basically transforming from a tooth-like appearance to one single mountain.

Another way to look at it is that it compares items of distance 1, then items of distance 2 and 1 again, then distance 4, 2 and 1 and so on until the list is sorted.

The big disadvantage of Bitonic Sort is that it can only sort arrays whose length is a power of 2. So inputting an array of size 10 will sort the leftmost 8 items and the other two will either be gibberish or out of order. To solve this, we can do bitonic sorts on different sublists and then merge them together. These sublists are generated by getting the highest power of 2 that fits, and then finding the highest power that fits in the remainder until it is filled; basically it's kind of like the binary expansion of the array's length.

Once each list is bitonically-sorted, it is merged using In-place Merge Sort since that method takes advantage of pre-sorted sublists.

**Time complexity**

The time complexity for Bitonic Sort is hard to pinpoint because it is split into the bitonic part and the merge part, an it also needs to use Sigma notation to express the time it takes.

To calculate the time complexity of the bitonic part, we use this function:

O((*f*(n/2)\*2\*1 + *f*(n/4)\*4\*2 + *f*(n/8)\*8\*3 + ... + *f*(n/(2^k))\*(2^k)\*k)/2 )

Where `2^k` is the highest power of 2 that fits inside the length and *f* is the floor function.

Next is the merge section. It's a lot simpler since it's mostly just linear with O(4n) as the maximum time, but it can perform a bit faster in a best-case scenario. Note that it would be O(0.9nlog(n)) without the bitonic pre-sorting.

So the overall time complexity is:

O((*f*(n/2)\*2\*1 + *f*(n/4)\*4\*2 + *f*(n/8)\*8\*3 + ... + *f*(n/(2^k))\*(2^k)\*k)/2 + 4n)

### Pairwise sorting network

This is just a variation of Batcher's Odd-even Mergesort. I don't really know much about it and I don't really care about it.

Time complexity O(0.8\*n^2). It makes big jumps upwards at every power of (2^k), and then a step down at every (1.5\*k^2)

### Odd-Even Sort

Compares odd pairs and even pairs. The odd/even comparisons can be done simultaneously so this can be speed-optimised by threading.

Time complexity is the same as Bubble Sort: O((n^2-n)/2 - [Perm])

### Sample Sort

This is a variation of Quick Sort that uses multiple pivots instead of just the one. It makes it a bit more complicated to perform, but helps reduce the worst-case tail end that Quick Sort has.

Sample sort checks to see if the list is small. If it is, it uses Insertion Sort instead.

For larger values, it starts out by picking out a few items at random to use as pivots. It then sorts them recursively.

The rest of the items are then placed in between their respective pivots one at a time. This version does this in-place but it is normally done using a bucket-like recursion. In-place means that you have to do a lot of swaps each time you place an item, but it keeps everything a lot neater.

At this point, we have a bunch of pivots that are in their correct position and a bunch of correctly unsorted items between them. These unsorted items between pivots are then sorted recursively using Sample Sort.

The big question at this point is "How many pivots do we use?". Well For this example I used n/4 pivots since that works pretty well. More pivots would mean that the initial pivot sort would be longer and placing each item would require more steps in the binary search; but on the other hand it would make the final recursive step a lot quicker since there would be smaller gaps between pivots.

Time complexity for this version is O(n\*log(n) - n/2). This makes it slightly better than Quick Sort on average. However remember that the in-place edit uses a lot of swaps to shift items along so it is quite swap-expensive. If this matters, a bucket-like recursion would be better.