/* ====================================================================================
	  ____
	 / ___|___  _ __ ___  _ __ ___   ___  _ __    ___
	| |   / _ \| '_ ` _ \| '_ ` _ \ / _ \| '_ \  / __|
	| |__| (_) | | | | | | | | | | | (_) | | | || (__
	 \____\___/|_| |_| |_|_| |_| |_|\___/|_| |_(_\___|

	This just holds a few common functions that are used among various files. Most of
	them are for examining arrays and their properties, but there are a few others.

 ==================================================================================== */

#include "common.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

// Print only if the --verbose flag is set
void printv(const char *format, ...) {
	if (v) {
		va_list args;
		va_start(args, format);
		vprintf(format, args);
		va_end(args);
	}
	
}

// Return a random number between 0 and max
int randInt (int max, int seed) {
	struct timeval current_time;
	gettimeofday(&current_time, NULL);
	srand ((unsigned)(current_time.tv_sec+current_time.tv_usec+seed));
	return (rand() % max);
}