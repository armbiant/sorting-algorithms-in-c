#ifndef _ARGPARSE_H_
#define _ARGPARSE_H_

#include <stdint.h>

// Extern variables

extern int repeats;
extern char exportType;
extern char algoFunc;

//
// Function prototypes
//

// Parses input
uint8_t argParse(int argc, char** argv);

#endif // _ARGPARSE_H_