#ifndef _COMMONSORT_H_
#define _COMMONSORT_H_

#include <stdint.h>

// This file contains libraries, prototypes and common vars

#define EXPORT_NONE 0
#define EXPORT_BASIC 1
#define EXPORT_DETAILED 2
#define ALGO_COUNT 46

//
// Common variables
//

extern int v;

//
// Function prototypes
//

// For only printing something in verbose mode. Use like printf();
void printv(const char *format, ...);

// Returns a random integer
int randInt(int max, int seed);

#endif // _COMMONSORT_H_