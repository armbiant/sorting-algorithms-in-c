#ifndef _ALGOTABLE_H_
#define _ALGOTABLE_H_

#include <stdint.h>

#define ALGOTABLE_TYPES 7
#define ALGOTABLE_ROWS 46
#define ALGOTABLE_COLUMNS 5
#define DEF_MAX 10000

#define ALGOTYPE_EXCHANGE		0
#define ALGOTYPE_INSERTION		1
#define ALGOTYPE_SELECTION		2
#define ALGOTYPE_RANDOM			3
#define ALGOTYPE_MERGE			4
#define ALGOTYPE_DISTRIBUTION	5
#define ALGOTYPE_CONCURRENT		6

typedef struct {
	uint64_t(*func)(int*, int);
	char* name;
	char* funcName;
	char type;
	uint32_t max;
} algoRow;

typedef struct {
	algoRow rows[ALGOTABLE_ROWS];
	char* columns[ALGOTABLE_COLUMNS];
	char len;
} algoTable;

// Shared table
extern algoTable listOfAlgos;
extern char* algoTypes[];

// Function prototypes
char getAlgoIndex(char* funcName);
uint64_t (*getAlgoPointer(char* funcName))(int*, int);
char* getAlgoName(char* funcName);

#endif // _ALGOTABLE_H_