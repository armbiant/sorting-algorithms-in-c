/* ==========================================================================
	    _    _             _        _     _            
	   / \  | | __ _  ___ | |_ __ _| |__ | | ___   ___ 
	  / _ \ | |/ _` |/ _ \| __/ _` | '_ \| |/ _ \ / __|
	 / ___ \| | (_| | (_) | || (_| | |_) | |  __/| (__ 
	/_/   \_\_|\__, |\___/ \__\__,_|_.__/|_|\___(_)___|
	           |___/

	This is a table that contains all the information about each algorithm,
	used to help with easy retrieval of algorithms both in terms of functions
	and their metadata.

 ========================================================================== */

#include "algoTable.h"
#include "algos/concurrentSort.h"
#include "algos/distributionSort.h"
#include "algos/exchangeSort.h"
#include "algos/insertionSort.h"
#include "algos/mergeSort.h"
#include "algos/randomSort.h"
#include "algos/selectionSort.h"

#include <stdio.h>

// Define types
char* algoTypes[] = {
	"Exchange Sorts",
	"Insertion Sorts",
	"Selection Sorts",
	"Random Sorts",
	"Merge Sorts",
	"Distribution Sorts",
	"Concurrent Sorts"
};

// Define the algo table
algoTable listOfAlgos = {
	.len  = ALGOTABLE_ROWS,
	.columns = {
		"Algorithm Name", "Function Name", "Function Pointer", "Agorithm type", "Max size"
	},
	.rows = {
		// Exchange Sorts
		{.name = "Bubble Sort",					.funcName = "bubbleSort",		.func = &bubbleSort,		.type = ALGOTYPE_EXCHANGE,		.max = DEF_MAX},
		{.name = "Comb Sort",					.funcName = "combSort",			.func = &combSort,			.type = ALGOTYPE_EXCHANGE,		.max = DEF_MAX},
		{.name = "Cocktail Sort",				.funcName = "cocktailSort",		.func = &cocktailSort,		.type = ALGOTYPE_EXCHANGE,		.max = DEF_MAX},
		{.name = "Proportional Exchange Sort",	.funcName = "pexSort",			.func = &pexSort,			.type = ALGOTYPE_EXCHANGE,		.max = DEF_MAX},
		{.name = "Quick Sort",					.funcName = "quickSort",		.func = &quickSort,			.type = ALGOTYPE_EXCHANGE,		.max = DEF_MAX},
		{.name = "Slow Sort",					.funcName = "slowSort",			.func = &slowSort,			.type = ALGOTYPE_EXCHANGE,		.max = 100},
		{.name = "Stooge Sort",					.funcName = "stoogeSort",		.func = &stoogeSort,		.type = ALGOTYPE_EXCHANGE,		.max = 200},

		// Insertion Sorts
		{.name = "Gnome Sort",					.funcName = "gnomeSort",		.func = &gnomeSort,			.type = ALGOTYPE_INSERTION,		.max = DEF_MAX},
		{.name = "Insertion Sort",				.funcName = "insertionSort",	.func = &insertionSort,		.type = ALGOTYPE_INSERTION,		.max = DEF_MAX},
		{.name = "Library Sort",				.funcName = "librarySort",		.func = &librarySort,		.type = ALGOTYPE_INSERTION,		.max = DEF_MAX},
		{.name = "Patience Sort",				.funcName = "patienceSort",		.func = &patienceSort,		.type = ALGOTYPE_INSERTION,		.max = DEF_MAX},
		{.name = "Shell Sort",					.funcName = "shellSort",		.func = &shellSort,			.type = ALGOTYPE_INSERTION,		.max = DEF_MAX},
		{.name = "Splay Sort",					.funcName = "splaySort",		.func = &splaySort,			.type = ALGOTYPE_INSERTION,		.max = DEF_MAX},
		{.name = "Tree Sort",					.funcName = "treeSort",			.func = &treeSort,			.type = ALGOTYPE_INSERTION,		.max = DEF_MAX},

		// Selection sorts
		{.name = "Bingo Sort",					.funcName = "bingoSort",		.func = &bingoSort,			.type = ALGOTYPE_SELECTION,		.max = DEF_MAX},
		{.name = "Cartesian Sort",				.funcName = "cartesianSort",	.func = &cartesianSort,		.type = ALGOTYPE_SELECTION,		.max = DEF_MAX},
		{.name = "Cycle Sort",					.funcName = "cycleSort",		.func = &cycleSort,			.type = ALGOTYPE_SELECTION,		.max = DEF_MAX},
		{.name = "Heap Sort",					.funcName = "heapSort",			.func = &heapSort,			.type = ALGOTYPE_SELECTION,		.max = DEF_MAX},
		{.name = "Selection Sort",				.funcName = "selectionSort",	.func = &selectionSort,		.type = ALGOTYPE_SELECTION,		.max = DEF_MAX},
		{.name = "Smooth Sort",					.funcName = "smoothSort",		.func = &smoothSort,		.type = ALGOTYPE_SELECTION,		.max = DEF_MAX},
		{.name = "Tournament Sort",				.funcName = "tournamentSort",	.func = &tournamentSort,	.type = ALGOTYPE_SELECTION,		.max = DEF_MAX},
		{.name = "Weak Heap Sort",				.funcName = "weakHeapSort",		.func = &weakHeapSort,		.type = ALGOTYPE_SELECTION,		.max = DEF_MAX},

		// Random/permutation sorts
		{.name = "Bogo Sort",					.funcName = "bogoSort",			.func = &bogoSort,			.type = ALGOTYPE_RANDOM,		.max = 5},
		{.name = "Dumb Bozo Sort",				.funcName = "dumbBozoSort",		.func = &dumbBozoSort,		.type = ALGOTYPE_RANDOM,		.max = 5},
		{.name = "Smart Bozo Sort",				.funcName = "smartBozoSort",	.func = &smartBozoSort,		.type = ALGOTYPE_RANDOM,		.max = 10},
		{.name = "Brute Force Sort",			.funcName = "bruteForceSort",	.func = &bruteForceSort,	.type = ALGOTYPE_RANDOM,		.max = 10},
		{.name = "Goro Sort",					.funcName = "goroSort",			.func = &goroSort,			.type = ALGOTYPE_RANDOM,		.max = 200},

		// Merge sorts
		{.name = "Merge Sort",					.funcName = "mergeSort",		.func = &mergeSort,			.type = ALGOTYPE_MERGE,			.max = DEF_MAX},
		{.name = "In-place Merge Sort",			.funcName = "inPlaceMergeSort",	.func=&inPlaceMergeSort,	.type = ALGOTYPE_MERGE,			.max = DEF_MAX},

		// Distribution sorts
		{.name = "American Flag Sort",			.funcName = "americanFlagSort",	.func = &americanFlagSort,	.type = ALGOTYPE_DISTRIBUTION,	.max = DEF_MAX},
		{.name = "Fast Bead Sort",				.funcName = "beadSortFast",		.func = &beadSortFast,		.type = ALGOTYPE_DISTRIBUTION,	.max = DEF_MAX},
		{.name = "Slow Bead Sort",				.funcName = "beadSortSlow",		.func = &beadSortSlow,		.type = ALGOTYPE_DISTRIBUTION,	.max = 150},
		{.name = "Burst Sort",					.funcName = "burstSort",		.func = &burstSort,			.type = ALGOTYPE_DISTRIBUTION,	.max = DEF_MAX},
		{.name = "Bucket Sort",					.funcName = "bucketSort",		.func = &bucketSort,		.type = ALGOTYPE_DISTRIBUTION,	.max = DEF_MAX},
		{.name = "Counting Sort",				.funcName = "countingSort",		.func = &countingSort,		.type = ALGOTYPE_DISTRIBUTION,	.max = DEF_MAX},
		{.name = "Flash Sort",					.funcName = "flashSort",		.func = &flashSort,			.type = ALGOTYPE_DISTRIBUTION,	.max = DEF_MAX},
		{.name = "Interpolation Sort",			.funcName = "interpolationSort",.func = &interpolationSort,	.type = ALGOTYPE_DISTRIBUTION,	.max = DEF_MAX},
		{.name = "Pigeonhole Sort",				.funcName = "pigeonholeSort",	.func = &pigeonholeSort,	.type = ALGOTYPE_DISTRIBUTION,	.max = DEF_MAX},
		{.name = "Proxmap Sort",				.funcName = "proxmapSort",		.func = &proxmapSort,		.type = ALGOTYPE_DISTRIBUTION,	.max = DEF_MAX},
		{.name = "Radix Sort (LSD)",			.funcName = "radixSortLsd",		.func = &radixSortLsd,		.type = ALGOTYPE_DISTRIBUTION,	.max = DEF_MAX},
		{.name = "Radix Sort (MSD)",			.funcName = "radixSortMsd",		.func = &radixSortMsd,		.type = ALGOTYPE_DISTRIBUTION,	.max = DEF_MAX},

		// Concurrent sorts
		{.name = "Batcher's Odd-even Sort",		.funcName = "batcherSort",		.func = &batcherSort,		.type = ALGOTYPE_CONCURRENT,	.max = DEF_MAX},
		{.name = "Bitonic Sort",				.funcName = "bitonicSort",		.func = &bitonicSort,		.type = ALGOTYPE_CONCURRENT,	.max = DEF_MAX},
		{.name = "Odd-even Sort",				.funcName = "oddEvenSort",		.func = &oddEvenSort,		.type = ALGOTYPE_CONCURRENT,	.max = 0},
		{.name = "Pairwise Network Sort",		.funcName = "pairwiseSort",		.func = &pairwiseSort,		.type = ALGOTYPE_CONCURRENT,	.max = DEF_MAX},
		{.name = "Sample Sort",					.funcName = "sampleSort",		.func = &sampleSort,		.type = ALGOTYPE_CONCURRENT,	.max = DEF_MAX},
	},
};

void algoFindFailure(char* funcName) {
	printf("Couldn't find function \"%s\". Here is a list of acceptable function names:\n", funcName);
	for (int i=0; i<ALGOTABLE_ROWS; i++) {
		printf("%s\n",listOfAlgos.rows[i].funcName);
	}
	printf("Exiting...\n");
	exit(EXIT_FAILURE);
}

// Returns the index of a function given its name
char getAlgoIndex(char* funcName) {
	for (int i=0; i<ALGOTABLE_ROWS; i++) {
		if(!strcmp(listOfAlgos.rows[i].funcName, funcName)) {
			return i;
		}
	}
	algoFindFailure(funcName);
}

// Returns a pointer to the function
// Sample use: c = getAlgoPointer(algo)(plist, listLen);
uint64_t (*getAlgoPointer(char* funcName))(int*, int) {
	return listOfAlgos.rows[getAlgoIndex(funcName)].func;
}

// Returns a string containing the function's full name
char* getAlgoName(char* funcName) {
	return listOfAlgos.rows[getAlgoIndex(funcName)].name;
}