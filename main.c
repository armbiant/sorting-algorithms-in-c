/* ====================================================================================
	 __  __       _
	|  \/  | __ _(_)_ __    ___
	| |\/| |/ _` | | '_ \  / __|
	| |  | | (_| | | | | || (__
	|_|  |_|\__,_|_|_| |_(_\___|

	This is the entry point to the rest of the program. It basically just directs the
	program towards the other sections such as argParse. It can also run a simple
	algorithm for testing/debugging purposes.

 ==================================================================================== */

#include "main.h"
#include "algoTable.h"
#include "argparse.h"
#include "common.h"
#include "data-structures/array.h"
#include "export.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

// Set some global values
int v = 0;				// Verbosity flag
int repeats = 0;		// Repetition flag
char exportType = 0;	// Export type
char algoFunc = 0;		// Default sorting algorithm


int main(int argc, char** argv) {

	int listLen;
	int i;
	int* plist;
	uint64_t resultsArray[SORT_TESTS];

	// Parse the input
	argParse(argc, argv);
	printv("Verbosity activated.\n");
	printv("Value repetition: %s\n", repeats ? "On" : "Off");

	// If no file is set for export
	if (!exportType) {

		// Take user input
		printf("Input a length: ");
		if (DEBUG) {
			listLen = 10;
		} else {
			scanf("%d", &listLen);
		}

		// Generate array
		printf("List length: %i\n", listLen);
		plist = generateRepeatingArray(plist, listLen);
		// For testing a specific permutation of integers
		int t[] = {};
		for (i=0; i<listLen; i++) {
			//plist[i] = repeats ? randInt (listLen-1, i) : i;
		}
		// Loop a few times to get an average score
		for (i=0; i<SORT_TESTS; i++) {
			printf("Loop #%i \r",i);
			shuffle(plist, listLen, i);
			printList(plist, listLen);
			resultsArray[i] = (*listOfAlgos.rows[algoFunc].func)(plist, listLen);
			printList(plist, listLen);
			checkList(plist, listLen, 1);
		}
		printf("Average time for %s: %i\n", listOfAlgos.rows[algoFunc].name, arrayAverage(resultsArray, SORT_TESTS));
		free(plist);
	} else {
		// Proceed with export
		exportToCsv();
	}
	return 0;
}
