CFLAGS=-I. -g -lm
SRC=$(wildcard *.c)
ALGOS=$(wildcard algos/*.c)
STRUCTS=$(wildcard data-structures/*.c)

default: CC=gcc
default: $(SRC) $(ALGOS) $(STRUCTS)
	$(CC) -o sort.exe $^ $(CFLAGS)

windows: CC=x86_64-w64-mingw32-gcc
windows: $(SRC) $(ALGOS) $(STRUCTS)
	$(CC) -o sort-$@.exe $^ $(CFLAGS)

clean:
	rm -rf \*.o